#!/usr/bin/php

<?php
# Sole function is to retrieve the last filename in an FTP directory.
# This will typically be the highest file in a numerical or alpha-
# betical list.  Used to use this with gentoo linux mirrors, in order
# to get the latest portage snapshot.  Now, most mirrors do this
# automatically.

# argv[1] = ftp server name
# argv[2] = path & glob of names to get (remember the glob)!
$filePath=$argv[2];

$ftpServer = str_replace( "ftp://", "", $argv[1] );
$ftpServer = substr( $ftpServer, 0, strpos( $ftpServer, "/" ) );

$connID=ftp_connect( $ftpServer );
ftp_login( $connID, "anonymous", "" ); 
$listing = ftp_nlist( $connID, $filePath );
$listingBit=array_pop( $listing );

$fullURL = ( $ftpServer .  $listingBit );
system( "wget -o log.txt -c {$fullURL}" );
print( $fullURL );
?>
