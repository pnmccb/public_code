<?php

/*

11/09/04 - 1st recorded revision.  Still not complete, but mostly 
           working (PNM).

11/12/04 - Added delete functionality (PNM).

11/13/04 - Fixed delete function.  Added tentative error handling (PNM).

4/1/06   - Finished the darn thing.  Put refreshes in place after the additions/deletions
           so that it's not so dumb.

4/9/06   - Moved some stuff around.  Created a master includes file (includes.php), added dates
           to topic index.

4/11/06  - Put "number of entries" listing in main wiki index for each topic.

4/16/06  - Started to add mimetypes feature


6/7/06   - Cleaned up SQL injection stuff

6/12/06	 - More SQL injection fixes

6/17/06  - Search function

10/5/06  - htmlspecialchars additions

7/07     - CSS style makeover ( font, background, textboxes )

11/12    - CSS conversion from tables to DIV

*/

require_once( "includes.php" );

$db    = "clawshos_wiki";
$host  = "localhost";
$port  = "3306";
$user  = "clawshos_wikiuser";
$pass  = "demo99";

$dbRes = connectDB( $host . ":" . $port, $user, $pass, $db );

/* Make sure we don't cut off any text: */
$_POST[ 'entry_text' ] = htmlspecialchars( $_POST[ 'entry_text' ] );
$_POST[ 'wiki_topic_text' ] = htmlspecialchars( $_POST[ 'wiki_topic_text' ] );
sanitizeInput();

switch( $_POST[ 'wikiaction' ] ) {
   
   case "addnewentry":
      
      wikiNew( "savenewentry", sprintf( "%d", $_POST[ 'wikitopicid' ] ) );
   
   break;

   case "delentry":
      
      wikiDeleteEntry( sprintf( "%d", $_POST[ 'wikitopicid' ] ) );
     
   break;
   
   case "changeentry":
   
      print( "Change goes here." );
   
   break;

   case "pushattachment":
      
      wikiSpawnDownload( $null, $null );
   
   break;
   
   case "showdetail":
      
      if( sprintf( "%d", $_POST[ 'topicID' ] ) == 0 )
	 wikiNew( "savenew", 0 );
      else
	 wikiDetail( sprintf( "%d", $_POST[ 'topicID' ] ) );
   
   break;

   case "savenew":
      wikiSaveEntry( wikiSaveTopic( ) );
   
   break;

   case "savenewentry":
      
      wikiSaveEntry( sprintf( "%d", $_POST[ 'wikitopicid' ] ) );
   
   break;

   case "showsearchform":
      
      print( wikiForm( "dosearch", "s", "", "Search" ) );
   
   break;

   case "dosearch":
   
      //$qt = "SELECT  print_r( $_POST );
      die();
   
   break;

   default:
      
      wikiIndex( );
    
   break;

}

function wikiPageHeader( $section, $sheetName ) {
   
   $return = ("<head>
               <title>
		  Wiki Database - {$section}
	       </title>
		  <link rel='stylesheet' href='{$sheetName}'>
	       </head>");
   
   return $return;

}

function wikiDeleteEntry( $topicEntryID ) {
   global $db;
   
   /* Find the parent topic id, for the next step */
   $query = mysql_query( "SELECT id,topic_id from {$db}.wiki_entry WHERE id='{$topicEntryID}' LIMIT 1" );
   $topicID = array_pop( mysql_fetch_row( $query ) );
   /**/
   
   $query = "DELETE FROM {$db}.wiki_entry WHERE id='{$topicEntryID}' LIMIT 1";
   
   $successFlag = 0; /* Yuck */
   if( mysql_query( $query ) ) {

      /* Is the topic empty of entries? */
      
      $query = "SELECT id,topic_id FROM {$db}.wiki_entry WHERE 
                topic_id='{$topicID}' and 1=1";
      
      if( mysql_num_rows( mysql_query( $query ) ) == 0 ) {

         /* If so, delete the topic */
	 
	 $q = "DELETE FROM {$db}.wiki_topic WHERE id='{$topicID}' LIMIT 1";
	 if( mysql_query( $q ) )
	    $successFlag += 1; 
      
      }
      else
	 $successFlag += 1;
      
      if( $successFlag == 1 ) { 
         header( "Refresh: 2; URL=\"{$myUrl}\"" );
	 print( "<h5>Entry deleted successfully.</h5>" );
      }
      else
	 errorMessage( );
   
   }
   else
      errorMessage( );
      
}

function wikiSaveTopic( ) {
   global $db;
   
   /* Saves topic and returns the resulting id for adding
      entries under that topic.
   */
   
   $date = getUnixtime( );

   $topicQuery = "INSERT INTO {$db}.wiki_topic SET id='',
                                                         text='{$_POST[ 'wiki_topic_text' ]}',
		                                         date='{$date}'";
							 
   $topicResult = mysql_query( $topicQuery );

   return mysql_insert_id( );

}

function wikiSaveEntry( $topicID ) {
   global $db;
   
   /* Put new entry into database */
   
   $date = getUnixtime( );
   
   if( $topicID > 0 ) { /* Error handling...should never be zero */
      $contentQuery = "INSERT INTO {$db}.wiki_entry SET id='',
       					                topic_id='{$topicID}',
					                text='{$_POST[ 'entry_text' ]}',
							date='{$date}'";
						  
      $contentResult = mysql_query( $contentQuery );
   }
/*      
      
      if( isset( $_FILES ) && $_FILES[ 'error' ] == 0 ) {

         //uploadFile( $_FILES[ 'entry_file'], mysql_insert_id(), "wiki", "wiki_upload" );
         uploadFile( $_FILES[ 'entry_file'], 1, "wiki", "wiki_upload" );
	 
      }
      if( isset( $_FILES[ 'error' ] ) ) { die( $_FILES[ 'error' ] ); }
   
   }
*/

   if( $topicID > 0 && $contentResult ) { 
      header( "Refresh: 2; URL=\"{$_SERVER[ 'PHP_SELF' ]}\"" );
      print( "<h5>Entry " .mysql_insert_id() . " added successfully.</h5>\n" );
   }
   else
      errorMessage();

}

function wikiForm( $action, $topicID, $formData, $buttonText = "Add" ) {
   /* Draws the actual input form and related Javascript so that data can be entered/edited */
   
   /* Use JS to enter return data in hidden form vars, so we can use
      POST method.  The check is currently broken in regard to
      the handling of the textarea (always returns true, to allow
      form to work.
   */
   
   $returnVal = ( "<script type='text/javascript' language='Javascript'>\n
           <!---\n
	   function validateThis() {\n
	      if( document.new_wiki.wiki_topic_text.value &&
	          document.new_wiki.entry_text.value ) {\n
	          
		  document.new_wiki.wikiaction.value='{$action}';\n
		  document.new_wiki.submit();\n
	      }\n
	      else {\n
	          alert( 'Missing form value!' );\n
	      }\n
	   }\n
	   //-->\n
	   </script>\n" );

   /* The formdata[] array contains two indexes, 0 == wiki topic text, 
      and 1 == wiki content text for the current topic
   */
   
   //$buttonText = ( strlen( $formData[ 1 ] ) == 0 ) ? "Add" : "Modify"; /* (Think that'll get it) */

   if( $topicID < 1 )
      $formData[ 0 ] = "(new entry)";
   else
      $disabledFlag = "DISABLED";
      
   
   $returnVal .= ( "<FORM name='new_wiki' 
                 method='post' 
		 action='{$_SERVER[ 'PHP_SELF' ]}'
		 enctype='multipart/form-data'>\n
           <DIV style='width:840px;'>
	      <SPAN style='float:left;'>
	         <INPUT TYPE='text' NAME='wiki_topic_text' VALUE='{$formData[0]}' {$disabledFlag} >
	      </SPAN>
              <SPAN style='float:right;'>
	          <INPUT TYPE='button' VALUE='{$buttonText}' onClick='validateThis()'>
	      </SPAN>\n
	      <SPAN style='float:clear;'>
                 <TEXTAREA name='entry_text' class='newentry'
		           rows='10'
		           cols='80'>{$formData[ 1 ]}</TEXTAREA> 
	          <INPUT TYPE='hidden' NAME='wikiaction' VALUE=''>
	          <INPUT TYPE='hidden' NAME='wikitopicid' VALUE='{$topicID}'>
              </SPAN>
	   </DIV>" );
	    
/* Save for later
	    <TR>
	       <TD>
	          Upload Content:
	       </TD>
	       <TD>
	          <INPUT TYPE='file' NAME='entry_file'>
		  <INPUT TYPE='hidden' NAME='MAX_FILE_SIZE' VALUE='20000000'>
	       </TD>
	    </TR>
*/
   return( $returnVal );
}

function wikiNew( $action, $topicID ) {
   global $db;
   
   /* Create a new topic */
   
   if( $topicID > 0 ) {
      $topicQ = "SELECT id,text,date FROM {$db}.wiki_topic WHERE id='{$topicID}' and 1=1";
      $topic = mysql_fetch_assoc( mysql_query( $topicQ ) );
   
   }
  
   print( "<HTML>\n" .   
           wikiPageHeader( "New Entry", "wiki.css" ) .
           "<BODY>\n" );
  
   /* Topic won't have detail, since it's **new** */
   print( wikiForm( $action, $topicID, array( $topic[ 'text' ], "" ) ) );

   print( "</BODY></HTML>\n" );

}

function wikiIndex( ) {
   global $db;
   
   /* Displays topics in database */
   
   $res = mysql_query( "SELECT id,text,date FROM {$db}.wiki_topic ORDER BY text" );
   
   print( "<HTML>\n" .
           wikiPageHeader( "New Entry", "wiki.css" ) .
          "<BODY>\n" );
  
   while ( $row = mysql_fetch_assoc( $res ) ) {
   
      $numEntries = mysql_num_rows( mysql_query( "SELECT id,topic_id FROM {$db}.wiki_entry 
                                                  WHERE topic_id='{$row[ 'id' ]}' ORDER BY id DESC" ) );
      print( "<DIV style='width:840px;'>
                 <A CLASS='topic' HREF=\"javascript:jumpLink('{$row[ 'id' ]}')\">
		        {$row['text']}
		 </A> ( " .
		     showTimeStamp( $row[ 'date' ] ) . ", " . $numEntries . " entries )
	    </DIV>\n
	    ");
   }
   
   print( "<P>\n" );
   print( "<A CLASS='topic' HREF=\"javascript:jumpLink('0')\">New wiki topic</A>\n" );
   print( "<A CLASS='topic' HREF=\"javascript:jumpLink('s')\">Search</A>\n" );

   print( "<FORM ACTION='{$_SERVER[ 'PHP_SELF' ]}' METHOD='post' NAME='wikiIndex'>\n
           <INPUT TYPE='hidden' NAME='wikiaction'>\n
           <INPUT TYPE='hidden' NAME='topicID'>\n
           </FORM>\n" );
   
   print( "<script type='text/javascript' language='Javascript'>\n
           <!---\n
	      function jumpLink( linkID ) {\n
	         document.wikiIndex.topicID.value = linkID;\n
		 
		 if( linkID == 's' ) {\n
		    document.wikiIndex.wikiaction.value = 'showsearchform';\n
		 }\n
		 else {
		    document.wikiIndex.wikiaction.value = 'showdetail';\n
		 }\n
		 document.wikiIndex.submit();\n
	      }\n
	   -->\n
	   </script>\n
	   \n" );

   print( "</BODY></HTML>\n" );


}

function wikiDetail( $indexID ) {
   global $db;
   
   /* Displays entries in wiki database according to topic. */
   
   $indexID = sprintf( "%d", $indexID );
   
   $detailQ = "SELECT id,topic_id,text,date FROM {$db}.wiki_entry WHERE topic_id='{$indexID}' ORDER BY id DESC";
   $detailRes = mysql_query( $detailQ );

   
   $topicQ = "SELECT id,text,date FROM {$db}.wiki_topic WHERE id='{$indexID}' and 1=1"; 
   $topic = mysql_fetch_array( mysql_query( $topicQ ) );
   
   $numEntries = mysql_num_rows( $detailRes );
   $dateStamp = showTimeStamp( $topic[ 'date' ] );
   
   print( "<HTML>\n" . 
          wikiPageHeader( "Topic Headings", "wiki.css" ) . 
          "<BODY>\n" 
        );  

   
   /* Set up the form for the topic heading, and the JS for the
      various functions.
   */
   

   print( "<FORM action='' method='post' name='wikiDetail'>
	   <INPUT type='hidden' name='wikitopicid' value=''>
           <INPUT type='hidden' name='wikiaction' value=''>
	   </FORM>
	    <DIV style='width:840px;'>
	       <SPAN class='header_div'>
		  {$topic[ 'text' ]}
	       </SPAN>
	       <SPAN class='header_div'>
		  {$dateStamp}
	       </SPAN>
	       <SPAN class='header_div'>
		  {$numEntries} entries
	       </SPAN>
	       <SPAN class='rightbutton_div'>
		  <A HREF='{$_SERVER[ 'PHP_SELF' ]}'>Main</A>
	       </SPAN>
	       <SPAN class='rightbutton_div'>
		  <A HREF='javascript:newEntry({$indexID})'>Add new entry</A>
	       </SPAN>
	    </DIV>\n" );
   
   /* Now for the entries, sorted in reverse order (newest first) */
   
   $rowNum = $numEntries;
   while ( $row = mysql_fetch_array( $detailRes ) ) {
       
      $text = stripslashes( $row[ 'text' ] ) . "\n";
      $dateStamp = showTimeStamp( $row[ 'date' ] );
      print( "<DIV class='spacer'>&nbsp;</DIV>\n" );

	 print( "<DIV style='width:840px;'>
		     <SPAN class='header_div'>
			Entry {$rowNum}/{$numEntries}, ${dateStamp}
		     </SPAN>
		     <SPAN class='rightbutton_div'>
			<A HREF='javascript:editEntry({$row[ 'id' ]})'>Edit</A>
		     </SPAN>
		     <SPAN class='rightbutton_div'>
			<A HREF='javascript:deleteEntry({$row[ 'id' ]})'>Delete</A>
		     </SPAN>
		     <SPAN class='rightbutton_div'>
			<A HREF='javascript:explodeEntry(\"entry{$row[ 'id' ]}\")'>Show in new window</A>
		     </SPAN>
		     <TEXTAREA class='newentry' ROWS='20' COLS='80' NAME='entry{$row[ 'id' ]}'>{$text}</TEXTAREA>
		  </DIV>
	       \n" );

      --$rowNum;
   
   }

   print( "<script type='text/javascript' language='Javascript'>
           <!---
	   function newEntry( id ) {
	      document.wikiDetail.wikiaction.value = 'addnewentry';
	      document.wikiDetail.wikitopicid.value = '{$indexID}';
	      document.wikiDetail.submit();
	   }

	   function editEntry( id ) {
	      document.wikiDetail.wikiaction.value = 'changeentry';
	      document.wikiDetail.wikitopicid.value = '{$indexID}';
	      document.wikiDetail.submit();
	   }
	   
	   function explodeEntry( textAreaName ) {
	      win = window.open( \"menubar=no,location=no,resizable=no,scrollbars=yes,status=no,width=400,height=300,screenX=150,screenY=150,top=150,left=150\" );
	      win.document.writeln( '<PRE>' );
	      win.document.writeln( document.getElementsByName( textAreaName )[0].value );
	      win.document.writeln( '</PRE>' );
	   }
	   
	   function deleteEntry( id ) {
	      document.wikiDetail.wikiaction.value = 'delentry';
	      document.wikiDetail.wikitopicid.value = id;
	      document.wikiDetail.submit();
	   }
	   
	   function pushDownload( id ) {
	      document.wikiDetail.wikiaction.value = 'delentry';
	      document.wikiDetail.wikitopicid.value = id;
	      document.wikiDetail.submit();
	   }
	   -->
	   </script>\n" );
   

   print( "</BODY></HTML>\n" );
}


function wikiSearchEntry( ) {
   print(""); 
}

function wikiSpawnDownload( $data, $contentType ) {
echo "for later.";
}

?>
