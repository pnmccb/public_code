#!/usr/bin/perl -W

# A later example, much more simple and less cumbersome.
# I tried at one point to figure out the current date and
# then generate a filename based on that.  This requires
# much less effort.  If someone can streamline this so
# it runs as part of the bash shell, be my guest.

# This script uses curl.  Curl has the capability of printing
# a directory listing to STDOUT.  It's written in Perl be-
# cause Perl has better array/resource handling. It's used
# in a shell script pipe, similar to the following:

# curl -sl ftp://gentoo.chem.wisc.edu/gentoo/snapshots/ | /usr/local/bin/getsnapshot.pl

my @files = <STDIN>; # create an array from the directory listing.
my $q = "";

# Pop items off the bottom of the stack, since we're assuming that
# the files are coming in ascending alphabetical order.  Keep doing
# this until we encounter the first array item that ends in '.bz2'
# It will be left in $q.  If our assumption is correct, this will
# contain the filename of the latest Portage tree archive.
while( $q = pop( @files ) and $q !~ /.bz2\r\n$/ ) { }

# Trim newline characters from the directory entry, and print the
# result to stdout.
$q =~ s/\r\n$//g;
print $q;
