#!/usr/bin/env python

import glob
import os
import string
import tarfile
import bz2

ffPath = "/home/nickm/.mozilla/firefox/xrgm9hm1.default/"
outFileName = "/home/nickm/ff-archive.tar.bz2"
tmpFile = os.tempnam()
fileList = list()

for ( root, dirs, files ) in os.walk( ffPath ): 
    for fileName in files:
        globFile = os.path.join( root, fileName )
        if( globFile.find( "/Cache/" ) < 1 ):
            fileList.append( globFile )

arcFile = tarfile.TarFile( tmpFile, mode='w' )

for fileName in fileList:
    print( "Adding %s.." % ( fileName ) )
    arcFile.add( fileName )

arcFile.close()

arcFile = open( tmpFile, "rb" )
bzFile = open( outFileName, "wb" )
arcFileSize = os.path.getsize( tmpFile )
bytesProgress = 0

while( True ):
    buffer = arcFile.read( 1024 )

    bytesProgress += 1024
    percentComplete = float( ( 1.0 * bytesProgress / arcFileSize * 100 ) )

    if( percentComplete % 5 == 0 ):
        print( "%.2f percent complete." % percentComplete )
    
    if( buffer == "" ):
        break
    else:
        bzFile.write( bz2.compress( buffer ) )

arcFile.close()
bzFile.close()
os.remove( tmpFile )
    


    


