use Date::Calc qw( Delta_Days Add_Delta_Days );
use HTML::TokeParser;
use Text::Autoformat qw(autoformat break_wrap);
use HTTP::GHTTP 1.07 qw(:methods);
use DBI;
use vars qw( $curDate $dbi $displayDate @date @story @storyTitles );
use strict;
#use warnings

# Crawls slashdot.org for each past daily edition and shoves it into
# a mysql database for later searching.

# You can also specify a starting date and an ending date on the commandline,
# in DD-MM-YYY format, but script default is "yesterday."

sub doSQL($) {

# FUNCTION:
# inserts story bits into SQL database. 

# GLOBAL VARS:
# $dbi = global DBI database handle
# $displayDate = date of current day's stories, in YYYY-MM-DD format
 
# RETURNS:
# nothing.  But prints heading, story titles, number of stories to STDOUT as
# debug data

# ARGUMENTS:
# $_[0] is array of hashes:
#   array[ storynumber ]{title}    = story title
#   array[ storynumber ]{date}     = date in YYYY-MM-DD HH:MM(AM|PM) format
#   array[ storynumber ]{link}     = link to story on slashdot website
#   array[ storynumber ]{postedby} = story's author
#   array[ storynumber ]{dept}     = story's "from-the-department-of" bit
#   array[ storynumber ]{intro}    = story's front-page introduction text

# Database schema:
#
# -- 
# -- Table structure for table `stories`
# -- 
#
# DROP TABLE IF EXISTS `stories`;
# CREATE TABLE IF NOT EXISTS `stories` (
#   `date` int(11) unsigned NOT NULL default '0',
#   `title` text NOT NULL,
#   `link` tinytext NOT NULL,
#   `author` tinytext,
#   `department` tinytext NOT NULL,
#   `intro` text NOT NULL,
#   UNIQUE KEY  (`title`(250)),
#   KEY `date` (`date`),
#   FULLTEXT KEY `title` (`title`),
#   FULLTEXT KEY `intro` (`intro`)
# ) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;
#

	local *storyTitles = shift;
	my $k = 0;	    # Iterator for stories processed.
	my $l = 0;          # Iterator for stories actually inserted.
	my $handle;         # db handle
	my $query = "";     # query string
	my @row = ( );	    # holds test data from db
	
	# Note: This is mostly for diagnostics.  Prints the story title
	# once before checking for that story's existence in the database,
	# and if the story is actually inserted into the db, prints a status
	# message. 
	printf( "Stories for $displayDate\n" );
	    
	# 'pack "U0"' switches Perl's utf8 string flag to 'on'.
	# Otherwise, MySQL butchers any foreign language chars:
	while( $storyTitles[ $k ]->{title} ne "" ) { 
	   printf( "\t%s", $storyTitles[ $k ]->{title} );
	   printf( "\n\t%s", $storyTitles[ $k ]->{link} );
	 
	 $query = "SELECT title,date from slashdot.stories where title=?";
	 $handle = $dbi->prepare( $query );
	 $handle->execute( $storyTitles[ $k ]->{title} );
	 @row = $handle->fetchrow_array;
	 
	 if( ( $row[ 1 ] < 1 && $handle->rows > 0 ) ) { die "Problem with data..date is " . 
          $row[1] . "\n"; }
	 
	 if( $handle->rows < 1 ) {
	    $query = "SELECT unix_timestamp( ? )";
	    $handle = $dbi->prepare( $query );
	    $handle->execute( $storyTitles[ $k ]->{date} );
	    @row = $handle->fetchrow_array;

	    if( $row[ 0 ] < 1 ) { die "Problem with parsed date..was " . $row[0] . "\n"; }
	    
	    $query = "INSERT INTO slashdot.stories VALUES ( unix_timestamp( ? ), ?, ?, ?, ?, ?)";
	    $handle = $dbi->prepare( $query );
	    $handle->execute( $storyTitles[ $k ]->{date},
                            $storyTitles[ $k ]->{title},
                            $storyTitles[ $k ]->{link},
                            $storyTitles[ $k ]->{postedby},
                            $storyTitles[ $k ]->{dept},
                            $storyTitles[ $k ]->{intro} );
	    
	    # Status to stdout:
	    printf( "...inserted into db" );
	    $l++;

	 } 
	 printf( "\n\n" );
	 $storyTitles[ $k ] = { };
	 $k++;
	
      }

      printf( " %s stories, %s inserted.\n", $k, $l );
}

sub getPage {
# FUNCTION:
# fetches http page from slashdot, using user profile cookie.

# RETURNS:
# HTML index page, as raw HTML.

# ARGUMENTS:
# None, uses global $curDate which contains issue date, formatted as YYYYMMDD
    my $tries = 0;
    my $content = "";
    my $page = HTTP::GHTTP->new; 
    
    for( $tries = 0; $tries < 4; $tries++ ) {

	printf( "http://slashdot.org/index2.pl?startdate=%s\n", $curDate );

        $page->set_proxy( "http://172.16.3.1:2138" );
	$page->set_uri( 'http://slashdot.org/index2.pl?issue=' . $curDate );
	$page->set_type( METHOD_GET );
	# Cookie added 1/26/2012
	$page->set_header( "Cookie", "user=27277%3A%3AYF9KlerPeORlJux6GrbHMs" ); 
	 
	$page->set_header( "User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060902 Firefox/1.5.0.6" );
	$page->process_request;

	$content = $page->get_body();

	last if $content;

	printf( "Sleeping..\n" );
	sleep 10800; # 3 hours
    }

    printf( "Status: %s\n", $page->get_status() ) if defined $page;
   
    return( $content ) or die( $page->get_error() );

}

sub parseStory($) {
# FUNCTION:
# parses story into array of hashes containing story information.  See doSQL for hash content.

# GLOBAL VARS:
# $displayDate = date of current stories, in YYYY-MM-DD format

# RETURNS:
# array of hashes containing parsed story information

# ARGUMENTS:
# raw HTML page in $_[0]

    my $q = HTML::TokeParser->new( \$_[0] );

    my $storyNum = 0;  # index for array of hashes
    my $k = 0;         # generic iterator
    my $time = "";     # string used in building date/time string
    my $arr = "";      # temporary string
    local *story;      # ptr to array of hashes
    my $trimmed_text = "";
    
    # Skip down to articles (Slash is so nice and CSS compliant!!)
    while( my $parenTok = $q->get_tag( "a" ) ) {
	 
	if( defined( $parenTok->[1]{name} ) ) {
	    last if $parenTok->[1]{name} eq "articles";
	}
    }

    # Now, meat and potatoes:
      while( my $tok = $q->get_tag( "span", "div" ) ) {

	next unless defined( $tok->[1]{id} );

	if( $tok->[1]{id} =~ "title-" ) {
	    $arr = $q->get_tag( "a" );
	    
	    if( $arr->[1]{href} != /\.slashdot\.org\/story\// ) {
		$arr = $q->get_tag( "a" );
	    }

	    # Pack "U0" is to enable UTF8 byte.  Otherwise, MySQL mangles the text:
	    $story[ $storyNum ]->{title} = $q->get_trimmed_text( "/span" ) . pack "U0";
	    $story[ $storyNum ]->{link} = "http:" . $arr->[1]{href};

	 }
	
	elsif( $tok->[1]{class} eq "details" ) {
	    
	    # This section depends upon the date set to "YYYY-MM-DD HH:MM" format in Slash user 
	    # preferences 
	
	    # $1 = author
	    # $2 = date ( YYYY-MM-DD HH:MM )
	    # $3 = time
	    # $4 = department

	    $trimmed_text = $q->get_trimmed_text( "/div" );
	    
	    if( $trimmed_text =~ /Posted\W+by\W+(\w+)\W+on\W+(\d{4})-(\d{2}-\d{2})\W+(\d{2}:\d{2})\W+from\W+the\W+(\S+)/ ) {
	        
	       $story[ $storyNum ]->{postedby} = substr( $1, 0, 70 );
	       $story[ $storyNum ]->{date} = sprintf( "%s-%s %s", $2, $3, $4 );
	       $story[ $storyNum ]->{dept} = $5;

	    }

	    #printf( "POSTEDBY--> %s <--\n", $story[ $storyNum ]->{postedby} );
	    #printf( "DATE--> %s <--\n", $story[ $storyNum ]->{date} );
	    #printf( "DEPT--> %s <--\n", $story[ $storyNum ]->{dept} );
	
	}	
	elsif( $tok->[1]{id} =~ "text-" ) {
	   $story[ $storyNum ]->{intro} = "";
	    # Autoformat line break defaults to 72 chars:
	    $story[ $storyNum ]->{intro} = autoformat( $q->get_trimmed_text( "/div" ), {break=>break_wrap} ) . pack "U0";

	    #printf( "STORY TEXT--> %s <--\n", $story[ $storyNum ]->{intro} );
	
	}

	if( defined( $story[ $storyNum ]->{title} ) and 
	    defined( $story[ $storyNum ]->{link}  ) and
	    defined( $story[ $storyNum ]->{intro}  ) and
	    defined( $story[ $storyNum ]->{dept}  ) and
	    defined( $story[ $storyNum ]->{postedby}  ) and
	    defined( $story[ $storyNum ]->{date}  ) ) 
	    { 
	       $storyNum++;
               $story[ $storyNum ]->{title}="";
	    }

    }

    $story[ $storyNum ]->{title}="";
    
    return( \@story );

}

#>>> Taken from CPAN.  Custom butchering by me.
    $dbi=DBI->connect("DBI:mysql:database=slashdot;host=localhost;mysql_enable_utf8=1",
                      "slashdot", "sN0t", {RaiseError => 1});

    $dbi->do( "SET NAMES 'utf8'" ); # Stupid hack alert...
    our $displayDate;
    my @start = ();
    my @stop = ();
    
    if( ! defined( $ARGV[ 0 ] ) ) {
	@date = localtime( time() - 86400 );
	@start = @stop = ( $date[ 5 ] + 1900, $date[ 4 ] + 1, $date[ 3 ] );
    
    }
    else {
	
	@start = split( /[\-\.\/]/, $ARGV[0] );
	@stop  = split( /[\-\.\/]/, $ARGV[1] );
    
	unshift( @start, pop( @start ) ); # Add_Delta_Days expects YYYY-MM-DD
	unshift( @stop, pop( @stop ) );

    }

    my $j = Delta_Days(@start,@stop);

    for ( my $i = 0; $i <= $j; $i++ ) {
	my @date = Add_Delta_Days( @start,$i );
	$curDate = sprintf( "%4d%02d%02d", @date );
	$displayDate = sprintf( "%4d-%02d-%02d", @date );

	doSQL( parseStory( getPage() ) );
    } 
	
#>>>
