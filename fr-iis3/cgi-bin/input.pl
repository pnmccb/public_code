# FILE NAME: input.*
#   DESCRIP: the input program for e-reserves processing (Windows NT)
#      DATE: 7/99

# Copyright (C) 1999  Nick McCubbins
# Portions Copyright (C) 1998, 1999  Keith Van Cleave, Paul Dawson-Schmidt
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.
#   
# This program is distributed in the hope that it will be useful,  
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License in COPYRIGHT for more details.

use CGI;

#use integer;           Danger, Will Robinson! Don't use!!

require "config";
require "upload";
require "subs";

$q = new CGI;

if ($q->param('func') eq "list_courses") { &Search_for_courses; }

if ($q->param('func') eq "upload") { &Upload_module; }

if ($q->param('func') eq "list_readings") { &Search_for_readings; }

if ($q->param('func') eq "search_readings") { &Search_for_readings($q->param('course_index'), 18); }

if ($q->param('func') eq "edit_reading") { 
	&Get_reading($q->param('reading'));
	&Get_course($q->param('course_index'));
	&Create_reading(1); 
}

if ($q->param('unassociate_reading')) {
	if ($q->param('delete_reading1_confirm')) { &Unassociate_reading($q->param('course_index'), $q->param('reading_index')); }
	$q->delete('delete_reading1_confirm');
	&Create_reading(1);
}

if ($q->param('save_course1')) { 
#This also resaves:

my $code = "";

	if ($q->param('course_index') eq "") { $code = &Find_next_courseno; }
	else { $code = $q->param('course_index') };

	&Save_course($code); 
	$q = new CGI('');
	&Get_course($code);

	$q->param(-name=> 'course_index', -value=>$code);
	&Create_course(1);
}


if ($q->param('func') eq "edit_course") { 
	&Get_course($q->param('course'));
	&Create_course(1); 
}

if ($q->param('show_link')) { &Create_reading(1); }

if ($q->param('reset_reading')) { &Create_reading; }

if ($q->param('reset_course')) { &Create_course; }

if ($q->param('go_upload_file')) { &Do_upload; }

if ($q->param('delete_file')) {

	if ($q->param('delete_confirm')) { &Delete_file($q->param('reading_filename')); }
	else { &Upload_module; }

}

if ($q->param('reading_authname_search')) { &Search_for_readings($q->param('reading_authorname'), 3); } 

if ($q->param('reading_title_search')) { &Search_for_readings($q->param('reading_title'), 2); } 

if ($q->param('course_dept_search')) { &Search_for_courses($q->param('course_dept'), 0); } 

if ($q->param('course_cname_search')) { &Search_for_courses($q->param('course_name'), 2); } 

if ($q->param('assign_readings')) { &Create_reading(0); }

if ($q->param('delete_course1')) {
	if ($q->param('delete_course1_confirm')) { 
		&Delete_course($q->param('course_index'));
		&Create_course(0);
	} 
	else { &Create_course(1); }
}

if ($q->param('delete_reading1')) { 
	if ($q->param('delete_reading1_confirm')) { 
		&Delete_reading($q->param('reading_index'));
		&Create_reading(0);
	}
	else { &Create_reading(1); }
}

if ($q->param('resave_reading')) { 
	# This is a little misleading.  This button also saves the reading for the first time.
	&Resave_reading($q->param('reading_index'));
	&Create_reading;
}

if ($q->param('reassign_reading')) { 
	&Resave_reading();
	&Create_reading;
}


&Create_course;

#########################################################################################################
sub Create_course {

# $_[0] saves state (a 1 saves state, 0 clears fields)

if ($_[0] != 1) { 
	my $resave_code = $q->param('course_index');     # This is from the link on the list of courses (edit_course)
	$q= new CGI('');
	$q->param(-name=>'course_index' -value=>$resave_code);
}

print $q->header;
print $q->start_html(-title=>"Electronic Reserves Processing - " . $school_name,
			   -BGCOLOR=>$script_bgcolor,
			 -TEXT=>$script_textcolor,
			 -LINK=>$script_linkcolor,
			 -VLINK=>$script_vlinkcolor,
			 -ALINK=>$script_alinkcolor);


print <<END_HTML;

<FONT SIZE=+2>Electronic Reserves Processing</FONT>

<BR><I>Page 1 of 2: Enter or modify a course</I>
<HR>
<TABLE>
END_HTML

print $q->startform(-action=>$server_name . $script_url . $input_cgi,
		    -name=>'course_1');

print "<TR><TD>Course Department:</TD><TD>\n";
print $q->textfield(-name=>'course_dept');
print "</TD><TD>\n";
print $q->submit(-name=>'course_dept_search',
		 -value=>'Search');
print "</TD></TR>\n";

print "<TR><TD>Course code & number:</TD><TD>\n";
print $q->textfield(-name=>'course_code');
print "</TD></TR>\n";

print "<TR><TD>Course Name:</TD><TD>\n";
print $q->textfield(-name=>'course_name');
print "</TD><TD>\n";
print $q->submit(-name=>'course_cname_search',
		 -value=>'Search');
print "</TD></TR>\n";

print "<TR><TD>Semester:</TD><TD>";
print $q->popup_menu(-name=>'course_semester',
		     -values=>\@semesters);
print "</TD></TR>\n";   

print "<TR><TD>Year:</TD><TD>\n";
print $q->textfield(-name=>'course_year');
print "</TD></TR>\n";


print "<TR><TD>Instructor Name (Last, First):</TD><TD>\n";
print $q->textfield(-name=>'ins_name');
print "</TD></TR>\n";

print "</TABLE><HR>";

if ($q->param('course_index')) {
	print $q->hidden(-name=>'course_index',
			 -value=>$q->param('course_index'));

	print $q->submit(-name=>'save_course1',
			 -value=>'Resave ' . $q->param('course_code'));

	print $q->submit(-name=>'assign_readings',
			 -value=>'Assign readings');

	print $q->submit(-name=>'reset_course',
			-value=>'Reset');

	print $q->submit(-name=>'delete_course1',
			 -value=>'Delete this course');

	print $q->checkbox(-name=>'delete_course1_confirm',
			   -label=>'Confirm delete');

}

else {

	print $q->submit(-name=>'save_course1',
			 -value=>'Save course');
}


print $q->endform;

&Print_footer($q->param('course_index'));

print $q->end_html;

exit 0;

}

########################################################################
sub Create_reading {

# $_[0] is a reset flag (a 1 saves state)

if ($_[0] != 1) {
	my $save_course_index = $q->param('course_index');
	my $save_course_dept = $q->param('course_dept');
	my $save_course_name = $q->param('course_name');
	my $save_course_semester = $q->param('course_semester');
	my $save_course_year = $q->param('course_year');

	$q = new CGI('');
	$q->param(-name=>'course_index', -value=>$save_course_index);
	$q->param(-name=>'course_dept', -value=>$save_course_dept);
	$q->param(-name=>'course_name', -value=>$save_course_name);
	$q->param(-name=>'course_semester', -value=>$save_course_semester);
	$q->param(-name=>'course_year', -value=>$save_course_year);
}


&Read_files("$reading_location");
unshift @files_in_dir, "<none>";

if ( !defined($q->param('reserve_date_start'))) { $q->param(-name=>'reserve_date_start', -value=>&Generate_datecode('today')); }

print $q->header;
print $q->start_html(-title=>'Online Reserves Processing - ' . $school_name,
		     -BGCOLOR=>$script_bgcolor,
		     -TEXT=>$script_textcolor,
		     -LINK=>$script_linkcolor,
		     -VLINK=>$script_vlinkcolor,
		     -ALINK=>$script_alinkcolor);


print <<END_HTML;

<FONT SIZE=+2>Electronic Reserves Processing</FONT>
<BR><I>Page 2 of 2: Enter/modify a course reading</I>
<HR>
END_HTML

if ($q->param('course_index')) {
	print "<I>" , $q->param('course_dept'), " | ", $q->param('course_name'), " | ", $q->param('course_semester') . " " .
      	$q->param('course_year'), "</I><HR>\n";
}

if ($q->param('show_link')) {
	print &Create_link();
}

print $q->startform(-action=>$script_url . $input_cgi,
		    -name=>'reading_1');

print "Author(s) of the article or selection (Last Name, First Name):<BR>\n";
print $q->textfield(-name=>'reading_authorname',
		    -size=>'80');
print "\n";
print $q->submit(-name=>'reading_authname_search',
		 -value=>'Search');
print "<BR>\n";

print "Title of article or selection (No punctuation!):<BR>\n";
print $q->textfield(-name=>'reading_title',
		    -size=>'80');
print "\n";
print $q->submit(-name=>'reading_title_search',
		 -value=>'Search');
print "<BR>\n";

print "Journal or periodical title reading is from:<BR>\n";
print $q->textfield(-name=>'journal_name',
		    -size=>'80');
print "<BR>\n";

print "Volume (Number) Year (journals/periodicals only):<BR>\n";
print $q->textfield(-name=>'journal_yearvolnum',
		    -size=>'80');
print "<BR>\n";

print "Book title reading is from:<BR>\n";
print $q->textfield(-name=>'book_name',
		    -size=>'80');
print "<BR>\n";

print "Book authors or editors - if different from the article";
print " or selection (Last name, First Name):<BR>\n";
print $q->textfield(-name=>'book_authname',
		    -size=>'80');
print "<BR>\n";

print "Place: Publisher, Year -- for books only<BR>\n";
print $q->textfield(-name=>'book_placepubyear',
		    -size=>'80');
print "<BR>\n";

print "Pages copied/scanned<BR>\n";
print $q->textfield(-name=>'pages',
		    -size=>'20');
print "<BR>\n";

print "Note (Appears in bold in HTML link)<BR>\n";
print $q->textfield(-name=>'HTML_note',
		    -size=>'80');

print "<HR><B>Choose one:</B><P>\n";

print "File name: ";
print $q->popup_menu(-name=>'reading_filename',
		     -values=>\@files_in_dir,
		     -default=>'<none>');

print "<P><B>...OR...</B><P>";

print "WWW Url: ", $q->textfield(-name=>'HTML_URL');

if ($q->param('bool_includehtmlprefix') eq 'on') {
	print "<INPUT TYPE='checkbox' NAME='bool_includehtmlprefix' CHECKED>Check to include 'http://' prefix";
}
else {
	print "<INPUT TYPE='checkbox' NAME='bool_includehtmlprefix'>Check to include 'http://' prefix";

}
print "<HR>\n";

print "<P>Copyright: \n";
print $q->popup_menu(-name=>'copyright_type',
		     -values=>\@copyright_types);
print "\n ";

print "Fee: \n";
print $q->textfield(-name=>'use_fee');
print "\n ";

print "Time period for use: \n";
print $q->textfield(-name=>'use_timeperiod');
print "<BR>\n";

if ($q->param('course_index')) {
	print "<P>These fields should be formatted YYYYMMDD:<BR>\n";
	print "\n";

	print "Date to be read <I>or</I> Group:\n";
	print $q->textfield(-name=>'date_read',
		    -size=>'8');
	print "\n";

	print "Dates on Reserve:\n";
	print $q->textfield(-name=>'reserve_date_start',
		    -size=>'8');
	print "\n";

	print "to\n";
	print $q->textfield(-name=>'reserve_date_stop',
		    -size=>'8');
	print "\n";

}
else {
	print "<INPUT TYPE='hidden' NAME='date_read' VALUE='", 
		$q->param('date_read'), "'>";
	print "<INPUT TYPE='hidden' NAME='reserve_date_start' VALUE='", 
		$q->param('reserve_date_start'), "'>";
	print "<INPUT TYPE='hidden' NAME='reserve_date_stop' VALUE='", 
		$q->param('reserve_date_stop'), "'>";

}

print "<HR>\n";

print "<INPUT TYPE='hidden' NAME='course_index' VALUE='", 
	$q->param('course_index'), "'>";
print "<INPUT TYPE='hidden' NAME='reading_index' VALUE='", 
	$q->param('reading_index'), "'>";
print "<INPUT TYPE='hidden' NAME='course_dept' VALUE='",  
	$q->param('course_dept') , "'>"; 
print "<INPUT TYPE='hidden' NAME='course_name' VALUE='",  
	$q->param('course_name'), "'>"; 
print "<INPUT TYPE='hidden' NAME='course_semester' VALUE='",  
	$q->param('course_semester'), "'>"; 
print "<INPUT TYPE='hidden' NAME='course_year' VALUE='",  
	$q->param('course_year'), "'>"; 

print $q->submit(-name=>'resave_reading',
		 -value=>'(Re)save reading');

print $q->submit(-name=>'new_course',
		 -value=>'New course');

print $q->submit(-name=>'reset_reading',
		-value=>'Reset');

print $q->submit(-name=>'show_link',
		-value=>'Show the link');

print $q->p;

print $q->submit(-name=>'delete_reading1',
		 -value=>'Delete this reading');

if ($q->param('course_index')) { print $q->submit(-name=>'unassociate_reading', -value=>'Disassociate reading from course'); }

print $q->checkbox(-name=>'delete_reading1_confirm',
		 -label=>'Confirm action');



print $q->endform;

&Print_footer($q->param('course_index'));

print $q->end_html;
exit 0;

}

###################################################################################################
sub Print_footer {

#$_[0] contains a course code, if any, to preserve state when 
#called from reading input screen

print "<HR>",
      "<A HREF='$server_name$script_url$input_cgi?func=list_courses'>See a list of courses</A>&nbsp;|&nbsp;",
      "<A HREF='$server_name$script_url$input_cgi?func=list_readings&course_index=$_[0]'>See a list of readings</A>&nbsp;|&nbsp;";

      if ($_[0]) { print "<A HREF='$server_name$script_url$input_cgi?func=search_readings&course_index=$_[0]'>See a list of readings for this course</A>&nbsp;|&nbsp;"; }
print "<A HREF='$server_name$script_url$input_cgi?func=upload' TARGET='_new'>Upload files</A>&nbsp;|&nbsp;",
      "<A HREF='$server_name$doc_url/input_help.html' TARGET='_new'>Some help</A>&nbsp;&nbsp;";
}

####################################################################################################
sub Get_course {

dbmopen (%COURSES_DB, "$data_location/data.courses", 0777) || die "Couldn't open $data_location/data.courses";

my @fields = split('\|\|', $COURSES_DB{"$_[0]"});

$q->param(-name=>'course_dept', -value=>$fields[0]);
$q->param(-name=>'course_code', -value=>$fields[1]);
$q->param(-name=>'course_name', -value=>$fields[2]);
$q->param(-name=>'course_semester', -value=>$fields[3]);
$q->param(-name=>'course_year', -value=>$fields[4]);
$q->param(-name=>'ins_name', -value=> $fields[5]);
$q->param(-name=>'course_index', -value=>$_[0]);

dbmclose %COURSES_DB;
}

########################################################################################################
sub Get_reading {

# $_[0] is the reading_index to retrieve, complete with number

my $save_course_index = "";

dbmclose %database; # Just to be sure
dbmclose %READINGS_DB;
dbmclose %COURSES_DB;

dbmopen (%READINGS_DB, "$data_location/data.readings", 0777) || die "Couldn't open $data_location/data.readings";
# dbmopen (%COURSES_DB, "$data_location/data.courses", 0777) || die "Couldn't open $data_location/data.courses";

$save_course_index = $q->param('course_index');

my @reading_fields = split('\|\|', $READINGS_DB{"$_[0]"});

$q = new CGI('');

$q->param(-name=>'reading_title', value=>$reading_fields[0]),
$q->param(-name=>'journal_name', value=>$reading_fields[1]),
$q->param(-name=>'journal_yearvolnum', value=>$reading_fields[2]),
$q->param(-name=>'reading_authorname', value=>$reading_fields[3]), 
$q->param(-name=>'book_name', value=>$reading_fields[4]),
$q->param(-name=>'book_authname', value=>$reading_fields[5]),
$q->param(-name=>'book_placepubyear', value=>$reading_fields[6]),
$q->param(-name=>'pages', value=>$reading_fields[7]),
$q->param(-name=>'HTML_note', value=>$reading_fields[8]),
$q->param(-name=>'reading_filename', value=>$reading_fields[9]),
$q->param(-name=>'HTML_URL', value=>$reading_fields[10]),
$q->param(-name=>'bool_includehtmlprefix', value=>$reading_fields[11]),
$q->param(-name=>'copyright_type', value=>$reading_fields[12]),
$q->param(-name=>'use_fee', value=>$reading_fields[13]),
$q->param(-name=>'use_timeperiod', value=>$reading_fields[14]),
$q->param(-name=>'date_read', value=>$reading_fields[15]),
$q->param(-name=>'reserve_date_start', value=>$reading_fields[16]),
$q->param(-name=>'reserve_date_stop', value=>$reading_fields[17]),
$q->param(-name=>'course_index', value=>$save_course_index),
$q->param(-name=>'reading_index', value=>$_[0]);

dbmclose %READINGS_DB;

if ($save_course_index) {
	dbmopen(%INDEX_DB, "$data_location/data.index",0777);
	
	my @index_fields = split('\|\|', $INDEX_DB{"$_[0]-$save_course_index"});

	$q->param(-name=>'date_read', value=>$index_fields[0]),
	$q->param(-name=>'reserve_date_start', value=>$index_fields[1]),
	$q->param(-name=>'reserve_date_stop', value=>$index_fields[2]);

	dbmclose %INDEX_DB;
}

}

#############################################################################################
sub Search_for_readings {

# $_[0] query data
# $_[1] field offset

dbmopen (%READINGS_DB, "$data_location/data.readings", 0777);

if ($q->param('course_index')) {
	dbmopen (%COURSES_DB, "$data_location/data.courses", 0777);

	my $temp_course_code = $q->param('course_index');

	@course_data = split('\|\|', $COURSES_DB{"$temp_course_code"});	

	dbmclose %COURSES_DB;	
}

my $number_of_readings = scalar (keys %READINGS_DB);
my $query = $_[0];
my $field_number = $_[1];
my $null = "";

if ($_[1] == 18) {
	dbmopen (%INDEX_DB, "$data_location/data.index", 0777);

	while (($index_key, $index_value) = each %INDEX_DB) {
		($reading, $course) = split('-', $index_key);

		if ($course eq $_[0]) { 
			@fields = split('\|\|', $READINGS_DB{"$reading"});	
			push @readings_to_display, "$fields[0]||$reading"; 
		}
	}

	dbmclose %INDEX_DB;      
}
else {
	while (($key, $value) = each %READINGS_DB) {

		@fields = split('\|\|', $value);
		# If it's a reading that we want, push necessary data into an array for sorting 
		# ('reading title' for sorting, 'key' so we can find it later:
		if (grep(/$query/, $fields[$field_number])) { push @readings_to_display, "$fields[0]||$key"; }
	}
}


my $readings_found = scalar (@readings_to_display);

print $q->header;
print $q->start_html(-title=>'Electronic Reserves Processing - ' . $school_name,
		     -BGCOLOR=>$script_bgcolor,
		     -TEXT=>$script_textcolor,
		     -LINK=>$script_linkcolor,
		     -VLINK=>$script_vlinkcolor,
		     -ALINK=>$script_alinkcolor);

print "<FONT SIZE=+2><A HREF='$server_name$script_url$input_cgi'>Electronic Reserves Processing</A></FONT>";
print "\n<HR SIZE=10>\n";
if (@course_data) {
	print "<I>", $course_data[0], "&nbsp;|&nbsp;", $course_data[2], "&nbsp;|&nbsp;", $course_data[3], " ", $course_data[4],
	      "</I><P>\n";
}

print "<TABLE BORDER=1>\n";

print "<TR><TD COLSPAN=2><B>$readings_found</B> found of <B>", $number_of_readings, 
      "</B> readings in the database. Follow the link to modify.</TD><TR>\n";

	foreach $item (sort {$a cmp $b} @readings_to_display) {
		($null, $reading) = split('\|\|', $item);
		@fields = split('\|\|', $READINGS_DB{"$reading"});

	print "<TR><TD VALIGN=TOP><A HREF='$server_name$script_url$input_cgi?func=edit_reading&reading=", $reading, "&course_index=", 
			$q->param('course_index'), "'>", $reading, "</A></TD><TD VALIGN=TOP>", &Format_reading(@fields, $reading),
	      "</TD></TR>";

	}

dbmclose %READINGS_DB;      

print "<TR><TD VALIGN=TOP COLSPAN=2 ALIGN=CENTER><A HREF='$server_name$script_url$input_cgi?func=edit_reading&course=",
      $q->param('course_index'),"'>Create a new reading</A></TD></TR>\n";

print "</TABLE>\n";
print $q->end_html;
exit 0;
}

#############################################################################################
sub Search_for_courses {

# $_[0] query data
# $_[1] field offset

dbmopen (%COURSES_DB, "$data_location/data.courses", 0777);
	
my $query = $_[0];
my $field_number = $_[1];
my $number_of_courses = scalar(keys %COURSES_DB);

while (($key, $value) = each %COURSES_DB) {
	@fields = split('\|\|', $value);
	if (grep(/$query/, $fields[$field_number])) { push @courses_to_display, $key; }
}
my $courses_found = scalar (@courses_to_display);

print $q->header;
print $q->start_html(-title=>'Electronic Reserves Processing - ' . $school_name,
		     -BGCOLOR=>$script_bgcolor,
			 -TEXT=>$script_textcolor,
			 -LINK=>$script_linkcolor,
			 -VLINK=>$script_vlinkcolor,
			 -ALINK=>$script_alinkcolor);

print "<FONT SIZE=+2><A HREF='$server_name$script_url$input_cgi'>Electronic Reserves Processing</A></FONT>";
print "\n<HR SIZE=10>\n";

print "<B>$courses_found</B> found of <B>$number_of_courses</B> courses in the database. Follow the link to modify.<P>\n"; 

	foreach $course (sort {$a cmp $b} @courses_to_display) {
		@fields = split('\|\|', $COURSES_DB{"$course"});

		print $fields[0], " -- <A HREF='$server_name$script_url$input_cgi?func=edit_course&course=",
		      $course, "'>", $fields[1], "</A>&nbsp;<I>", $fields[2], "</I> -- ", $fields[3]," ",$fields[4],
		      " (ref: $course)<BR>";
	}

dbmclose %COURSES_DB;      
exit 0;
}

############################################################################

sub Save_course {

my $code = $_[0]; # Initialize vars

dbmopen(%COURSES_DB, "$data_location/data.courses",0777);

if ($code ne "") { 

	$COURSES_DB{"$code"} = join('||', $q->param('course_dept'), 
					   $q->param('course_code'), 
					   $q->param('course_name'),
					   $q->param('course_semester'),
					   $q->param('course_year'),
					   $q->param('ins_name'));
}

dbmclose %COURSES_DB;
}

#########################################################################################
sub Delete_course {

# $_[0] contains the doomed course's index

dbmopen(%COURSES_DB, "$data_location/data.courses",0777);
	delete $COURSES_DB {"$_[0]"};
dbmclose %COURSES_DB;

# Now we clear out all the course's indexes...

dbmopen(%INDEX_DB, "$data_location/data.index",0777);
	while (($key, $value) = each %INDEX_DB) {
		($reading, $course) = split('-', $key);

		if ($course eq $_[0]) { delete $INDEX_DB{"$key"}; }
	}
dbmclose %INDEX_DB;

}

#############################################################################################
sub Delete_reading {

# $_[0] contains the doomed reading's index

dbmopen(%READINGS_DB, "$data_location/data.readings",0777);
	delete $READINGS_DB {"$_[0]"};
dbmclose %READINGS_DB;

# Now we clear out all the reading's indexes...

dbmopen(%INDEX_DB, "$data_location/data.index",0777);
	while (($key, $value) = each %INDEX_DB) {
		($reading, $course) = split('-', $key);

		if ($reading eq $_[0]) { delete $INDEX_DB{"$key"}; }
	}

dbmclose %INDEX_DB;

}

#############################################################################################
sub Resave_reading {
# This sub has two modes: Resave, and save.
# $_[0] contains a reading index, if any (resave mode).  If none, generate a new one based 
# on course_index (save mode).

my $code="";

if ($_[0]) { $code = $_[0]; }
else { $code = &Find_next_readingno; }

dbmopen(%READINGS_DB, "$data_location/data.readings",0777);

my $paramval = $q->param('bool_includehtmlprefix');

#####################################################
# Somebody tell me why I have to do this clumsy shit?

if ($paramval ne 'on') { $checkbox_param = 'off' }
else { $checkbox_param = 'on' };

#####################################################

if ($code) {

	$READINGS_DB{"$code"} = join('||',  $q->param('reading_title'),                                         # 0
							$q->param('journal_name'),                              # 1
							$q->param('journal_yearvolnum'),                        # 2     
							$q->param('reading_authorname'),                        # 3
							$q->param('book_name'),                                 # 4
							$q->param('book_authname'),                             # 5
							$q->param('book_placepubyear'),                         # 6
							$q->param('pages'),                                     # 7
							$q->param('HTML_note'),                                 # 8
							$q->param('reading_filename'),                          # 9
							$q->param('HTML_URL'),                                  # 10
							$checkbox_param,                                        # 11
							$q->param('copyright_type'),                            # 12
							$q->param('use_fee'),                                   # 13
							$q->param('use_timeperiod'));                           # 14
}

dbmclose %READINGS_DB; 

# Save the dates data if we have a course associated:
if ($q->param('course_index') && $code) {
	dbmopen(%INDEX_DB, "$data_location/data.index",0777);
	

	$INDEX_DB{$code . "-" . $q->param('course_index')} = join ('||', $q->param('date_read'),   	  # 15
					       				 $q->param('reserve_date_start'), # 16
			        	 			         $q->param('reserve_date_stop')); # 17

	dbmclose %INDEX_DB;
}

}

############################################################################################################
sub Format_reading {

my $linkToShow="";
my $reading_link="";

  if ($_[3])
    { $linkToShow = $linkToShow . $_[3] . ", "; }

  $linkToShow = $linkToShow . "\"" . $_[0] . ".\" ";

  if ($_[1])
  {
    $linkToShow = $linkToShow . "<i>" . $_[1] . "</i> ";
    $linkToShow = $linkToShow . $_[2] . ": " . $_[7] . ".";
  }

  if ($_[4])
  {
    $linkToShow = $linkToShow . "in ";

    if ($_[5])
      { $linkToShow = $linkToShow . $_[5] . ", "; }

    $linkToShow = $linkToShow . '<i>' . $_[4] . '.</i> ';
    $linkToShow = $linkToShow . $_[6] . ":";
    $linkToShow = $linkToShow . $_[7] . ".";
  }

  if ($_[8])
    { $linkToShow = $linkToShow . "<b>" . $_[8] . "</b>"; }

  if ($_[9] && $_[9] ne "<none>" && -e "$reading_location/$_[9]") { $reading_link = "File: <I>$_[9] </I>"; }
  elsif ($_[10]) {
	if ($_[11] eq "on") {
		$reading_link = " Link: <a href='http://" . $_[10] . "'>http://$_[10]</a> "; 
	}
	else {
		$reading_link = " Link: <a href='" . $_[10] . "'>$_[10]</a>"; 
	}
	
  }

  if (!$_[10]) { $reading_link = $reading_link . " No URL" }
  if (!$_[9] or $_[9] eq "<none>") { $reading_link = $reading_link . " No File" }  

  $linkToShow = $linkToShow . $reading_link;

  return $linkToShow . "<P>";

}

####################################################################################################
sub Find_next_courseno {

my $lastindex = 0;

dbmopen(%COURSES_DB, "$data_location/data.courses",0777);
while (($key, $value) = each %COURSES_DB) {
	if ($value ne "") { ++$lastindex; }
}
dbmclose %COURSES_DB;

return "cr$lastindex";
}

####################################################################################################
sub Find_next_readingno {

my $lastindex = 0;

dbmopen(%READINGS_DB, "$data_location/data.readings",0777);

while (($key, $value) = each %READINGS_DB) {
	if ($value ne "") { ++$lastindex; }
}


dbmclose %READINGS_DB;

return "rr$lastindex";
}

####################################################################################################
sub Unassociate_reading {

dbmopen(%INDEX_DB, "$data_location/data.index",0777);

my $code = "$_[1]-$_[0]";

delete $INDEX_DB{"$code"};

dbmclose %INDEX_DB;

}