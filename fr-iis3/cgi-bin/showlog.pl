# FILE NAME: showlog.*
#   DESCRIP: logfile viewer/manipulator (Windows NT)
#      DATE: 7/99

# Copyright (C) 1999  Nick McCubbins
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.
#   
# This program is distributed in the hope that it will be useful,  
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License in COPYRIGHT for more details.


use CGI;
require "config";

$q=new CGI;

if ( $q->param('clear')) { 
	unlink "$logfile_location/logfile.txt" || die "Couldn't delete logfile $logfile_location/logfile.txt";
	open (LOGFILE, ">$logfile_location/logfile.txt") || die "Couldn't recreate logfile $logfile_location/logfile.txt";
	close LOGFILE;


	$q = new CGI('');
	print "Content-type: text/html\n\n";
	print "<HTML><HEAD>\n";
	print "<META HTTP-EQUIV='Refresh' CONTENT='1;";
	print " URL=$servername$doc_url'>";

	print "<TITLE>Log Deleted</TITLE></HEAD>\n";
	print "<BODY BGCOLOR='$script_bgcolor' TEXT='$script_textcolor' ";
	print "LINK='$script_linkcolor' VLINK='$script_vlinkcolor' ALINK='$script_alinkcolor'>\n";

	print "<H1>Log Deleted!</H1><P>Please wait for refresh.</BODY>";
	print "</HTML>\n";
	exit 0;

}


open(logFile, "$logfile_location/logfile.txt") || die "Can't read $logfile_location/logfile.txt";

print $q->header;
print $q->start_html(-title => 'Show log file');

print "<PRE>\n";

while (<logFile>) { print; }

print "</PRE>\n";

close logFile;

print $q->hr;
print $q->startform(-action=>$script_url . "/showlog.cgi");

print $q->submit(-name=>'clear',
		 -value=>'Clear Log');

print $q->end_form;
print $q->end_html;


