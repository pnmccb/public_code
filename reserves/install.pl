print "WARNING: Be sure you're running this script from the root of the distribution!!";
print "  Additionally, you should be Administrator to do this.\n";
print "If not, press CTRL-C to abort.\n\n";

print "Enter the name of the group that all Freereserves administrators belong to? [fr_users]: ";
$prompt = <STDIN>;
chop $prompt;

$prompt = "fr_users" if !$prompt;

if (!-e ".\\data") {
	print "Creading data dir...\n";
	system "mkdir data"; 
}
if (!-e ".\\logfiles") {
	print "Creading logfiles dir...\n";
	system "mkdir logfiles"; 
}

if (!-e ".\\readings") { 
	print "Creading readings dir...\n";
	system "mkdir readings"; 
}

system "echo y|cacls . /T /C /G Administrators:F \"$prompt\":C \"Guests\":R";
system "echo y|cacls .\\*.* /T /C /G Administrators:F \"$prompt\":C \"Guests\":R";
system "echo y|cacls .\\logfiles  /T /C /G Administrators:F \"$prompt\":C \"Guests\":C";
system "echo y|cacls .\\logfiles\\*.*  /T /C /G Administrators:F \"$prompt\":C \"Guests\":C";
