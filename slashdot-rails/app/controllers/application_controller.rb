class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index

    if params[:page]
      paginate
    else
      session[:filter] =  { 'start_date' => '',
                            'end_date'   => '',
                            'intro'      => '',
                            'title'      => ''  }
      session[:sortby] = 'date DESC'
    end

  end

  def new_search

    session[:filter].update params[:filter]
    session[:page] = 1

  end

  def paginate

    session[:page] = params[:page]

  end

  def display_results

    results = Stories.get_stories session[:sortby], session[:filter]

    @stories_list = results.paginate page: session[:page], per_page: 25

  end

  def render *args

    display_results
    @filter = session[:filter]
    super :index

  end

end
