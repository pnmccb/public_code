require 'active_support'
require 'test_helper'
require 'slashstory_fixtures'

class SlashStoryLibTest < ActiveSupport::TestCase

   test 'should retrieve yesterdays slashdot index page' do

     assert_not_empty ENV['LOGIN_COOKIE']

     content = Nokogiri.HTML SlashStory::get_page(Date.new(2016,3,15))

     title = content.css('title').text.downcase.index('news for nerds')
     # ensure that we're logged in:
     assert_not_empty content.css('a[href="//slashdot.org/~claws"]').text

     assert_not_nil title

   end

  test 'should parse a slashdot index page' do

    # Tests against the data automatically loaded into the db from the fixture

    fixtures_hashes = []
    parsed_stories = SlashStory::parse_stories SlashStoryFixtures::sample_index_file

    parsed_stories.each do |id, s|
      fixtures_hashes.push SlashStoryFixtures::generate_id_hash s[:title],
                                                                s[:author],
                                                                s[:dept],
                                                                s[:intro]
    end

    assert_equal fixtures_hashes.count, Stories.where(id_hash: fixtures_hashes).count

  end

  test 'should store parsed stories in sql db' do

    fixtures_hashes = []
    parsed_stories_fixtures = YAML.load_file("#{Rails.root}/test/fixtures/stories.yml")
    Stories.delete_all

    assert_equal 0, Stories.count

    parsed_stories_fixtures.each do |index, s|
      fixtures_hashes.push s[:id_hash].to_s
      s[:id_hash] = ''
    end

    SlashStory::do_sql parsed_stories_fixtures

    assert_equal fixtures_hashes.count, Stories.where(id_hash: fixtures_hashes).count

  end

  test 'should not store an existing story' do
    duplicate_story = {}
    count_before_test = Stories.count
    parsed_stories_fixtures = YAML.load_file("#{Rails.root}/test/fixtures/stories.yml")

    duplicate_story['82089611'] = parsed_stories_fixtures['82089611']
    duplicate_story['82089611'][:id_hash] = ''

    SlashStory::do_sql duplicate_story

    assert_equal count_before_test, Stories.count

  end

end
