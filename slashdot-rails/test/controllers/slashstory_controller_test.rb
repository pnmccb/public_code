require 'active_support'
require 'test_helper'

class ApplicationControllerTest < ActionController::TestCase

    test 'should load catalog index page' do
      ids_from_html = []

      get :index
      assert_equal 200, @response.status
      assert_select 'title', 'Slashdot Archive'

      body_tags = Nokogiri::HTML @response.body

      body_tags.css('table#results tr').each do |el|
        story_id = el['data-story-id']
        assert_not_nil story_id
        ids_from_html.push story_id
      end

      ids_from_db = Stories.order('date DESC').pluck('id')

      assert_equal 25, ids_from_html.count

      assert_nil ids_from_db <=> ids_from_html

    end

    test 'should load page 2 of catalog index page' do
      id_numbers = []
      session[:filter] =  { 'start_date' => '',
                            'end_date'   => '',
                            'intro'      => '',
                            'title'      => ''  }
      session[:sortby] = 'date DESC'

      get :index, { :page => 2 }
      assert_equal 200, @response.status
      assert_select 'title', 'Slashdot Archive'

      body_tags = Nokogiri::HTML @response.body

      body_tags.css('table#results tr').each do |el|
        story_id = el['data-story-id']
        assert_not_nil story_id
        id_numbers.push story_id
      end

      assert_equal 2, id_numbers.count

    end

end
