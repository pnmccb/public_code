class SlashStoryFixtures
   def self.sample_index_file
      html_file_name = "#{Rails.root}/test/fixtures/slashdot_index_page.html"

      return nil unless File.readable? html_file_name

      html_file_handle = File.open html_file_name, "r:iso-8859-1:utf-8"
      html_file_handle.read
   end

   def self.generate_id_hash(title, author, dept, introduction)
      Digest::SHA1.hexdigest title + author + dept + introduction
   end
end
