require 'slashStory'
require 'rufus/scheduler'

every_ten_minutes = Rufus::Scheduler.new
once_a_day        = Rufus::Scheduler.new
scheduled_time    = '00 01 * * * America/Chicago'

every_ten_minutes.every '10m', first_in: '5s' do
  Rails.logger.info '--MARK--'
end

once_a_day.cron(scheduled_time) do
  run_crawl_task
end

def run_crawl_task
  s = SlashStory.new

  s.do_sql(s.parse_stories(s.get_page(Date.yesterday)))
end