require 'slashStory'

  desc "Capture stories from Slashdot."
  task 'crawl', [:begin_date, :end_date] => :environment do |t, args|

    case args.to_hash.length
      when 0
        begin_date = Date.yesterday
        end_date   = begin_date

      when 1
        begin_date = Date.strptime args[:begin_date], '%m-%d-%Y'
        end_date   = Date.today

      when 2
        begin_date = Date.strptime args[:begin_date], '%m-%d-%Y'
        end_date   = Date.strptime args[:end_date], '%m-%d-%Y'
    end

    Rails.logger.info "Beginning date: #{begin_date.to_s}  Ending date: #{end_date.to_s}"

    while 1
      Rails.logger.info "Date: #{begin_date.to_s}"

      if begin_date <= end_date
        SlashStory::do_sql(SlashStory::parse_stories(SlashStory::get_page(begin_date)))
        begin_date = begin_date.tomorrow
      else
        break
      end
    end

  end