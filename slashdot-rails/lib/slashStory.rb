#!/bin/env ruby
# encoding: utf-8

require 'net/http'

class SlashStory
  def self.get_page(get_date)
    uri = URI.parse "https://slashdot.org/?issue=#{get_date.strftime('%Y%m%d')}"

    Rails.logger.info "RequestURL is #{uri}"

    req = Net::HTTP::Get.new uri
    req['Cookie'] = ENV['LOGIN_COOKIE']
    req['User-Agent'] = "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/0.9.8q"
    res = Net::HTTP.new uri.host, uri.port
    res.use_ssl = true
    response = res.request req

    response.body.encode('UTF-8', {:invalid => :replace, :undef => :replace, :replace => '?'})
  end

  def self.parse_stories(body)
    stories = Hash.new
    page = Nokogiri::HTML(body)

    page.css('span[id^=title-]').each do |l|
      link_info = l.css('a')
      link_id = l[:id].match(/^title-(.*)/)[1]

      stories[link_id] = {:id => link_id,
                          :link => 'http:' + link_info[0]['href'],
                          :title => link_info.text.strip}
    end

    page.css('div[id^=details-]').each do |l|
      link_id = l[:id].match(/^details-(.*)/)[1]
      stories[link_id][:author] = l.text.strip.match(/(Submitted|Posted)\s+by\s+(.*)/)[2]
      stories[link_id][:date] = l.css('time').text.match(/on (.*)/u)[1]
      stories[link_id][:dept] = l.css('span.dept-text').text.strip
    end

    page.css('div[id^=text-]').each do |l|
      link_id = l[:id].match(/^text-(.*)/)[1]
      stories[link_id][:intro] = l.text.strip
    end

    Rails.logger.info "#{stories.count} stories parsed."

    stories
  end

  def self.do_sql(stories)
    stories.each do |index, s|

      t =  Stories.create s.except(:id)

      indicator = t.valid? ? 'O' : 'X'

      puts "#{s[:title].to_s}   #{s[:link]} #{indicator}"
      puts "\n"
      Rails.logger.info "#{s[:title].to_s}   #{s[:link]} #{indicator}"
      Rails.logger.info "\n"

    end
  end
end
