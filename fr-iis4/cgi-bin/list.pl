# FILE NAME: list.*
#   DESCRIP: the patron processing program for the e-reserves (NT version)
#      DATE: 6/99

# Copyright (C) 1999  Nick McCubbins
# Portions Copyright (C) 1998, 1999  Keith Van Cleave, Paul Dawson-Schmidt
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.
#   
# This program is distributed in the hope that it will be useful,  
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License in COPYRIGHT for more details.

use CGI;

# Insert your authentication plugin here:
require "auth_id";

require "config";
require "subs";

$q = new CGI;

if ($q->param('func') eq "" &&
    $q->param('browse') eq "" &&
    $q->param('search') eq "" &&
    $q->param('course') eq "") { print $q->redirect(-URL=>$server_name . $doc_url); }

# Get here before we authenticate...
# hitting showlog will trigger the server's built-in login/password authentication
if ( $q->param('search') eq "log") { print $q->redirect(-URL=>$server_name . $script_url . $showlog_cgi); }

if (time < $q->param('sto')) {
	$ts = time + 21600;
}

# referrer_flag replaces $ENV{'HTTP_REFERER'). We need some way to tell that we've come
# from the main page. I've chosen a hidden field, because HTTP_REFERER is clumsy.
if ($q->param('referer_flag') || time > $q->param('sto')) {
        if (&know_user ne "fr_user"){
		&wrongID;
		&record_log("ACCESS DENIED FROM " . $ENV{"REMOTE_HOST"});
		exit 1;
	}
	else { $ts = time + 21600; } # if know_user succeeds.
}

if ($q->param("func") eq "get") {
	&record_log("SELECTED COURSE: " . $q->param("course"));
	&Display_course($q->param("course"));
	exit 0;
}

if ($q->param("func") eq "download") {
	&Download_reading($q->param("reading"));
	exit 0;
}

if ($q->param("browse") eq "courses" || $q->param("search")) {
	&record_log("(BROWSE COURSES)");
	&Browse_Courses;
	exit 0;
}

if ($q->param("browse") eq "instructors" && !$q->param("search")) {
	&record_log("(BROWSE INSTRUCTORS)");
	&Browse_Instructors;
	exit 0;
}

print "Content-type: text/html\n\n";
print "<HTML><HEAD>\n";
print "<META HTTP-EQUIV='Refresh' CONTENT='0;";
print " URL=$servername$script_url$list_cgi'>";
exit 0;

###########################################################################################
sub Browse_Courses {

dbmopen(%COURSES_DB, "$data_location/data.courses", 0777) or die "Couldn't open courses database $data_location/data.courses";
dbmopen(%READINGS_DB, "$data_location/data.readings", 0777) or die "Readings";

&Print_listing_header("Courses");

# First, get a list of courses and their departments:

my @departments="";

while (($key, $value) = each %COURSES_DB) {

	@course_data = split(/\|\|/, $value);
 
#                                                                                      <-look for duplicate departments----->                          
	if ($course_data[3] ne $current_semester || $course_data[4] ne $current_year || grep(/$course_data[0]/, @departments)) { next; }

	$search_string = uc($q->param('search'));

	if (!$search_string) {
		push @departments, $course_data[0];
	}
	else {
		if (grep(/$search_string/ , uc($course_data[0])) || grep(/$search_string/ , uc($course_data[1])) || grep(/$search_string/ , uc($course_data[2])) || grep(/$search_string/ , uc($course_data[5]))) {
			push @departments, $course_data[0];
		}
	}
	
}

if (length("@departments") <= 1) { print "<H3>Sorry, your search returned no results..</H3>"; }
else {

foreach $department (sort {$a cmp $b} @departments) {

	if ($save_dept ne $department ) {
		$save_dept = $department;
		print "</UL><B>$department</B><UL>\n";        
	}

	foreach $course_key (sort { $COURSES_DB{$a} cmp $COURSES_DB{$b} } keys %COURSES_DB) {
			
		@course_data=split('\|\|', $COURSES_DB{"$course_key"});                        
		if ($department eq $course_data[0] && @course_data) {
			
			print "<LI><A HREF='$servername$script_url$list_cgi?sto=$ts",
			"&func=get&course=$course_key'>", $course_data[1], "</A> (", $course_data[5],
			": ", $course_data[2], ")</LI>\n";
				
		}
			
	}
}
 
} # end of else
print "</UL>";
&Print_listing_footer;

dbmclose %COURSES_DB;
dbmclose %READINGS_DB;

exit 0;
}

##############################################################################################
sub Browse_Instructors {

dbmopen(%COURSES_DB, "$data_location/data.courses", 0777) or die "Can't open courses database";
dbmopen(%READINGS_DB, "$data_location/data.readings", 0777) or die "Can't open readings database";

&Print_listing_header("Instructors");

# First, get a list of instructors and their classes:

while (($key, $value) = each %COURSES_DB) {

	@course_data = split(/\|\|/, $value);

	if ($course_data[3] ne $current_semester || $course_data[4] ne $current_year) { next; }
	
	# Trims all the whitespace before and after the string. Dumb, I know.
	$course_data[5] = join('', split(' ', $course_data[5])); 

	# Now, put the space back in between last and first names:
	$course_data[5] =~ s/,/, /g;

	if (!grep(/$course_data[5]/, @classes)) {push @classes, $course_data[5]; }
}

if (length("@classes") <= 1) { print "<H3>Sorry, your search returned no results.</H3>"; }
else {

foreach $instructor (sort {$a cmp $b} @classes) {

	if ($save_ins ne $instructor ) {
		$save_ins = $instructor;
		print "</UL><B>$instructor</B><UL>\n";        
	}

	foreach $course_key (sort { $COURSES_DB{$a} cmp $COURSES_DB{$b} } keys %COURSES_DB) {

		@course_data=split('\|\|', $COURSES_DB{"$course_key"});                        

		if (grep(/$instructor/, $course_data[5])) {
			
			print "<LI><A HREF='$servername$script_url$list_cgi?sto=" , $ts,
			"&func=get&course=$course_key'>", $course_data[1], "</A> (", $course_data[5],
			": ", $course_data[2], ")</LI>\n";
				
		}
			
	}
}

} # end of else
print "</UL>";
&Print_listing_footer;

dbmclose %COURSES_DB;
dbmclose %READINGS_DB;

exit 0;
}


##############################################################################################
sub Display_course {

my $course_code = $_[0];

dbmopen(%COURSES_DB, "$data_location/data.courses", 0777) or die "Courses database could not be opened.";
dbmopen(%READINGS_DB, "$data_location/data.readings", 0777) or die "Readings database could not be opened.";
dbmopen(%INDEX_DB, "$data_location/data.index", 0777) or die "Index database could not be opened.";

my @course_data=split('\|\|', $COURSES_DB{"$course_code"});

print $q->header;
print $q->start_html(-title=>$course_data[0],
		     -BGCOLOR=>$script_bgcolor,
		     -TEXT=>$script_textcolor,
		     -LINK=>$script_linkcolor,
		     -VLINK=>$script_vlinkcolor,
		     -ALINK=>$script_alinkcolor);

&Print_searchform;

print "<BLOCKQUOTE>\n";
print "<FONT SIZE=+2>", $course_data[1], "&nbsp;&nbsp;", $course_data[2], "</FONT><BR>\n";
print $course_data[3], "&nbsp;&nbsp;", $course_data[4], "<BR>\n";
print "Instructor:&nbsp;", $course_data[5], "<BR>\n";
print "<P><B>Please note:</B> The files below may be very large, taking longer to download or print.<BR>\n";
print "Print copies are also available on reserve in the library.<HR>\n";


while (($key, $value) = each %INDEX_DB) {
	($reading_ref, $course_ref) = split('-', $key);

	if ($course_ref eq $course_code) {
		my @index_data = split('\|\|', $value);
		push @courses_to_display, "$index_data[0]||$reading_ref";
	}
}

if (length("@courses_to_display") <= 1) { print "<H2>Sorry, this course contains no readings..</H2>\n"; }

foreach $item (sort {$a cmp $b} @courses_to_display) {
  ($date, $reading) = split('\|\|', $item);
  my @reading_data = split('\|\|', $READINGS_DB{"$reading"});

  if (length ("@reading_data") <= 1) { next; }

  if ($date ne $saved_date) {
	$saved_date = $date;
	print "<B>", &Convert_date_to_string($date), "</B><BR>\n";
  }		

my $linkToShow="";
my $reading_link="";

  if ($reading_data[3])
    { $linkToShow = $linkToShow . $reading_data[3] . ", "; }

  $linkToShow = $linkToShow . "\"" . $reading_data[0] . ".\" ";

  if ($reading_data[1])
  {
    $linkToShow = $linkToShow . "<i>" . $reading_data[1] . "</i> ";
    $linkToShow = $linkToShow . $reading_data[2] . ": " . $reading_data[7] . ".";
  }

  if ($reading_data[4])
  {
    $linkToShow = $linkToShow . "in ";

    if ($reading_data[5])
      { $linkToShow = $linkToShow . $reading_data[5] . ", "; }

    $linkToShow = $linkToShow . '<i>' . $reading_data[4] . '.</i> ';
    $linkToShow = $linkToShow . $reading_data[6] . ":";
    $linkToShow = $linkToShow . $reading_data[7] . ".";
  }

  if ($reading_data[8])
    { $linkToShow = $linkToShow . "<b>" . $reading_data[8] . "</b>"; }

  $linkToShow = $linkToShow . " <br> ";

  if ($reading_data[9] && $reading_data[9] ne "<none>" && -e "$reading_location/$reading_data[9]") {
	my @index_data = split('\|\|', $INDEX_DB{"$reading-$_[0]"});

	if (&Check_date($index_data[1], $index_data[2]) == 1) {
		my $size = -s "$reading_location/$reading_data[9]";

		$reading_link = "<A HREF='$servername$script_url$list_cgi?sto=$ts" .
	        "&func=download&reading=$reading-$_[0]'>" . 
	        uc(substr($reading_data[9], rindex($reading_data[9], '.')+1)). "</A>&nbsp;(" . 
		$size . "&nbsp;bytes)";
	}
  }
  elsif ($reading_data[10]) {
	if ($reading_data[11] eq "on") {
		$reading_link = "Available at: <a href='http://" . $reading_data[10] . "'>WWW site</a> "; 
      	}
      	else {
		$reading_link = "Available at: <a href='" . $reading_data[10] . "'>WWW site</a>"; 
      	}
	
  }

  if (!$reading_link) { $linkToShow=$linkToShow . "<B>This item is not currently available through Electronic Reserves.</B>"; }
  else { $linkToShow = $linkToShow . $reading_link; }

  print "$linkToShow<BR><BR>\n";

}		

&Print_listing_footer;
print $q->end_html;
dbmclose %COURSES_DB;
dbmclose %READINGS_DB;
dbmclose %INDEX_DB;

exit 0;

}
###########################################################################################################
sub Check_date {

# $_[0] contains the starting date for the reading
# $_[1] contains the ending date

	$early_date_in_question = $_[0];
	$later_date_in_question = $_[1];

	$datestring = &Generate_datecode((localtime)[3], (localtime)[4], (localtime)[5]); 

	# If a date field is left blank, set to rediculous values to
	# allow any date:

	if (!$early_date_in_question) { $early_date_in_question = "00000000"; }
	if (!$later_date_in_question) { $later_date_in_question = "99999999"; }

	if ($later_date_in_question >= $datestring && $early_date_in_question <= $datestring) {
		return 1; #It's valid
	}
	else { return 2; }
}

#######################################################################################

############################################################################
# Most of this sub is taken from the original version
############################################################################
sub Download_reading {

# $_[0] contains the reading's code as seen in the reading link ( rr0-cr0 )

dbmopen(%READINGS_DB, "$data_location/data.readings", 0777) or die "Readings database could not be opened.";

$q = new CGI('');       # Resets browser for different filetype

my ($reading_ref, $course_ref) = split('-', $_[0]);

my @reading_data = split('\|\|', $READINGS_DB{"$reading_ref"});

my $file = $reading_data[9];

my $size = -s $reading_location . "/" . $file;

if ($contenttype eq "")
{ 
	# zip files ...
	if    (index($file,".zip")>0) {$contenttype = "application/x-zip-compressed";}
	# pdf's ...
	elsif (index($file,".pdf")>0) {$contenttype = "application/pdf";}
	elsif (index($file,".xls")>0) {$contenttype = "application/xls";}
	elsif (index($file,".ppt")>0) {$contenttype = "application/ppt";}
	elsif (index($file,".doc")>0) {$contenttype = "application/doc";}
	elsif (index($file,".wpd")>0) {$contenttype = "application/wpd";}
	# htm's ...
	else                          {$contenttype = "text/html";}
}

print "Content-Type: $contenttype\r\n";

if ($contenttype ne "text/html") {
	printf "Content-Length: %d\r\n", $size;
	print "Content-Disposition: attachment; filename=$file\r\n";
}

print "\r\n";

open(FILE, "< $reading_location/$file");
binmode FILE;		# for
binmode STDOUT;		# NT				  

while(read(FILE, $buffer, 4096)) { print STDOUT $buffer; }

close FILE;
dbmclose %READINGS_DB;

&record_log("PUSHED: $file BYTES: $size from reading " . $_[0] );
  
exit 0;

}
############################################################################
# Extracted from the HTML source:
############################################################################
sub Print_listing_header {

# $_[0] contains either "Courses" or "Instructors" (note capitalization)

print $q->header;
print $q->start_html(-title => $_[0] . ' on Electronic Reserves',
		     -BGCOLOR=>$script_bgcolor,
		     -TEXT=>$script_textcolor,
		     -LINK=>$script_linkcolor,
		     -VLINK=>$script_vlinkcolor,
		     -ALINK=>$script_alinkcolor);

&Print_searchform;

print "<BLOCKQUOTE><FONT SIZE=+1<B>", $_[0], " on Electronic Reserves</B></FONT><BR>\n";
print "Browse through this alphabetical list of ", lc($_[0]), " which have material accessible over the WWW.\n";
print "<HR NOSHADE WIDTH=95% ALIGN='left'>\n";

}

##############################################################################
# Extracted from the HTML source:
##############################################################################
sub Print_searchform {

print "<TABLE ALIGN='center'>\n";
print "<TR><TD><IMG SRC='$doc_url$graphicname' HEIGHT='133' WIDTH='400' ALT='Electronic Reserves'>\n";
print "</TD>\n";
print "<TD><TABLE BORDER CELLPADDING='3'><TR><TD BGCOLOR='fffff'><CENTER>Search Again?<BR>\n";
print "<FORM NAME='listForm' METHOD='post' ACTION='$servername$script_url$list_cgi'>\n";
print "<INPUT TYPE='hidden' NAME='sto' VALUE='$ts'>"; 

print "<INPUT TYPE='radio' NAME='browse' VALUE='courses' CHECKED>\n";
print "<FONT SIZE=-1>Browse Courses<INPUT TYPE='radio' NAME='browse' VALUE='instructors'>Browse Instructors<BR></FONT>\n";
print "<FONT SIZE=+1><I>OR</I></FONT><BR>Search:<INPUT NAME='search' SIZE='20'><P><INPUT TYPE='submit' VALUE='Submit'></B>\n";
print "</CENTER></TD></TR></TD></TABLE></FORM></TABLE></CENTER>\n";

}

############################################################################
sub Print_listing_footer {

print "<HR><P><ADDRESS>Please send questions or comments to\n";
print "<A HREF='mailto:", $email, "'>$email</A></ADDRESS></BLOCKQUOTE>\n";

}
