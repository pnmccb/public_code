/*  more04.c  - version 0.4 of more
 *	read and print lines then pause for a few special commands
 *      feature of version 0.4: reads from /dev/tty for commands
 *                              buffering off; echo off
 *                              uses the full window
 *  newmore.c - Version of more for CS306, section #1 due 3/24/03 (paul mccubbins)
 *      - Modified so that percentage is displayed for files (see diff)
 */
#include	<stdio.h>
#include        <termios.h>
#include        <sys/ioctl.h>
#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>

int     PAGELEN;

void do_more( FILE *);
int see_more( FILE *, double ); 
void set_cr_noecho_mode( int );
void tty_mode( int, int );
off_t getFileSize( FILE *);

int main( int ac , char *av[] )
{
	FILE	*fp;

	if ( ac == 1 )
		do_more( stdin );
	else
		while ( --ac )
			if ( (fp = fopen( *++av , "r" )) != NULL )
			{
				do_more( fp ) ; 
				fclose( fp );
			}
			else
				exit(1);
	return 0;
}

off_t getFileSize( FILE *fp ) {
   /* getFileSize( FILE *fp )
    * 
    * returns size of file pointed to by fp
    * or -1 on error.
    */
    
   		int 	fileDescriptor = fileno( fp );
   struct 	stat  	status;

   if( fileDescriptor )
      fstat( fileDescriptor, &status );
   else if( fp != stdin ) {
      perror( "getFileSize" );
      exit(1);
   }

   if( status.st_size )
      return( status.st_size );
   else
      return ( -1 );
}

void do_more( FILE *fp )
/*
 *  read PAGELEN lines, then call see_more() for further instructions
 */
{
	char		line[LINELEN];
	int		num_of_lines 	= 0;
	int		reply;
	FILE		*fp_tty;
        int     	fp_tty_fd;
	double 		fileSize 	= -1;
        struct 		winsize 	wbuf;
	double		currentFilePosition = -1;
	
      	fileSize = getFileSize( fp );
        
	fp_tty = fopen( "/dev/tty", "r" );	   /* NEW: cmd stream   */
	if ( fp_tty == NULL )			   /* if open fails     */
		exit(1);                           /* no use in running */

        fp_tty_fd = fileno(fp_tty);    /* CONVERT POINTER TO FILE DESCRIPTOR */    
        tty_mode(0, fp_tty_fd);        /* SAVE ORIGINAL SETTINGS             */
        set_cr_noecho_mode(fp_tty_fd); /* INSTALL NO BUFFERING AND NO ECHO   */
                                       /* FOR TERMINAL SETTINGS              */

        if ( ioctl(fp_tty_fd, TIOCGWINSZ, &wbuf) != -1 ) 
             PAGELEN = wbuf.ws_row - 1;
        else
             PAGELEN = 24;

	while ( fgets( line, LINELEN, fp ) ){		/* more input	*/
		if ( num_of_lines == PAGELEN ) {	/* full screen?	*/
		   if( fp != stdin )
		      currentFilePosition = ftell( fp ) / fileSize * 100;

		  /* NEW: pass FILE *  */
		  /* and figure out file length */
		  reply = see_more(fp_tty, currentFilePosition ); 
			
		  if ( reply == 0 )		     /*    n: done   */
			break;
		  
		  num_of_lines -= reply;	     /* reset count */
		
		}
		if ( fputs( line, stdout )  == EOF ) {	/* show line	*/
                        tty_mode(1, fp_tty_fd);         /* RESTORE SETTINGS */
			exit(1);			/* or die	*/
                }
		num_of_lines++;				/* count it	*/
	}
        
        tty_mode(1, fp_tty_fd);        // RESTORE SETTINGS
}

int see_more(FILE *cmd, double percentage )  /* NEW: accepts 2 args  */
/*
 *	print message, wait for response, return # of lines to advance
 *	q means no, space means yes, CR means one line
 */
{
	int	c;
	
	if( percentage > 1 )
	   printf("\033[7mmore (%0.0f%%)? \033[m", percentage );	/* reverse on a vt100	*/
	else
	   printf("\033[7mmore? \033[m");	/* reverse on a vt100	*/
	
	while( ( c = getc( cmd ) ) != EOF )	/* NEW: reads from tty  */
	{
		if ( c == 'q' )			/* q -> N		*/
			return 0;
		if ( c == ' ' )			/* ' ' => next page	*/
			return PAGELEN;		/* how many to show	*/
		if ( c == '\n' )		/* Enter key => 1 line	*/
			return 1;		
	}
	return 0;
}

void set_cr_noecho_mode(int fd) {

    struct termios ttystate;

    tcgetattr(fd, &ttystate);
 
    ttystate.c_lflag &= ~ICANON;
    ttystate.c_lflag &= ~ECHO;
    ttystate.c_cc[VMIN] = 1;

    tcsetattr(fd, TCSANOW, &ttystate);
}

void tty_mode(int how, int fd) {

   static struct termios original_state;

   if ( how == 0 )
       tcgetattr(fd, &original_state);
   else
       tcsetattr(fd, TCSANOW, &original_state);
}
