#include <fcntl.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <utmp.h>

void showEntry( struct utmp *entry );
void showTime( time_t *timeVar ); 

int main( int argc, char **argv ) {
   struct passwd *currentUser;
   struct utmp currentUtmpEntry;
   int fileDescriptor = -1;
   int bytesRead = 0;
   uid_t currentUID;

   printf( "%s\n", UTMP_FILE );

   fileDescriptor = open( UTMP_FILE, O_RDONLY );
   if( fileDescriptor == -1 ) {
      perror( UTMP_FILE );
      exit( 1 );
   }

   do {
      bytesRead = read( fileDescriptor, &currentUtmpEntry, sizeof( currentUtmpEntry ) );

      if( bytesRead == -1 )
	 perror( UTMP_FILE );

      if( bytesRead == 0 )
	 break;
      
      showEntry( &currentUtmpEntry );      
   
   } while( 1 ); 
   
   close( fileDescriptor );

}

void showTime( time_t *timeVar ) {
   
   char *cp = (char * )ctime( timeVar );

   printf( "%12.12s", cp+4 );
}

void showEntry( struct utmp *entry ) {
      
   if ( entry->ut_type == USER_PROCESS ) {
	 printf( "%-8.8s   ", entry->ut_name );
	 printf( "%-8.8s   ", entry->ut_line );
	 showTime( &entry->ut_time );
#ifdef SHOWHOST
	 printf( "   (%s)", entry->ut_host );
#endif
	 printf( "\n" );
      }

}
