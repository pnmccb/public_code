/* scroll.c, for CS 306 Lab#3 ( Spring '03 ) ( Paul McCubbins ) */
#include <errno.h>
#include <ncurses.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

void cleanup	   ( int mySignal );
int  createFileLock( int );
void getScreenSize ( void );
void myWindowResize( int );

int	SCREENCOLS;
int  	SCREENROWS;
int	infileFD = -1;
char	*outputString;
pid_t  	pidOfFork;

int main( int argc, char **argv ) {
   int 	  	    pipes[2];
   int		    currentArgvPos = 1;
   int		    targetColumn = 0;
   int		    i;
   int		    test;
   char  	    c;
   char		    *currentFilename;
   FILE		    *infileStream;
   struct sigaction sigHandler;

   /* Setup some signals for clean exit.             */
   /* ( It makes more sense after reading page 187 ) */
   for( i = 0; i < 13; i++ )
      signal( i, cleanup );
   
   signal( SIGTERM, cleanup );
   
   /* This shouldn't happen anyway */
   signal( SIGSEGV, SIG_IGN );

   /* Handle window resizings */
   sigHandler.sa_handler = myWindowResize;
   sigHandler.sa_flags = SA_RESTART;

   if( sigaction( SIGWINCH, &sigHandler, NULL ) == -1 )
      perror( "sigaction 1" );
   /* end */

   if( argc < 3 ) {
      fprintf( stderr, "Usage: %s <microseconds> <filename1> <filename2> ...\n", argv[0] );
      exit( 1 );
   }

   pipe( pipes );
   
   if( ( pidOfFork = fork() ) == -1 )
      perror( "Fork" );

   if( pidOfFork > 0 ) {

      close( pipes[0] );
      //dup2( pipes[1], 1 );

      while( 1 ) {
	 
	 if( read( infileFD, &c, 1 ) <= 0 ) {
	    /* If we get here, that means we've reached    */
	    /* EOF, so cycle to the next argument, or back */
	    /* to the first.                               */
	    close( infileFD );

	    write( pipes[1], "(EOF)", 5 );
	    
	    ++currentArgvPos;
	    if( currentArgvPos > argc - 1 ) {
	       currentArgvPos = 2;
	       write( pipes[1], "(EOS)", 5 ); /* EOS == End Of Sequence */
	    }

	    currentFilename = argv[ currentArgvPos ];
	    infileFD = open( currentFilename, O_RDONLY );

	    if( infileFD == -1 ) {
	       perror( "file open" );
	       close( pipes[1] );
	       close( pipes[0] );
	       exit( 1 );
	    }
	 
	    if( ( createFileLock( infileFD ) == -1 ) ) {
	       fprintf( stderr, "Could not get read lock on file.\n" );
	       close( infileFD );
	       close( pipes[1] );
	       close( pipes[0] );
	       exit( 1 );
	    }

	    infileStream = fdopen( infileFD, "r" );
	    setbuf( infileStream, NULL ); 	    /* Turn off buffering */
	 }
	 else {
	 
	    if( write( pipes[1], &c, 1 ) != 1 ) {
	       perror( "write to pipe" );
	       exit(1);
	    }
	    
	    usleep( atoi( argv[1] ) );
	 }
      }
   
      exit(0);
   }
   else {
   
/* This is the child.  It displays the characters from the pipe */
      //sleep(15);			/* Wait for parent to start sending... */
      close( pipes[1] );        
      //dup2( pipes[0], 0 );

      myWindowResize( SIGWINCH );

      while( 1 ) { 
	 /*
	    Shift the string one character to the left, to make
	    room for the next character to appear on the right 
	 */
	 if( outputString == NULL )
	    cleanup( SIGINT );
	 for( i = 1; i < SCREENCOLS; i++ ) 
	    outputString[ i ] = outputString[ i + 1 ];
      
	 if( ( test = read( pipes[0], &c, 1 ) < 1 ) && test != EOF ) {
	    perror( "read from pipe" );
	    exit( 1 );
	 }
	    
	 if( c < 32 || c > 126 )  	/* If it's not a normal character */ 
	    c = ' ';		        /* then change it to a space.     */
     
	 /* Put the new char on the end of the string */
	 if( outputString == NULL )
	    cleanup( SIGINT );
	 outputString[ SCREENCOLS - 1 ] = c;
	 outputString[ SCREENCOLS ] = '\0';
      
	 /* If we're not already at the left column, */
	 /* move 1 space to the left:                */
	 if( targetColumn == 0 )
	    targetColumn = 0;
	 else
	    targetColumn--;
      
	 move( SCREENROWS / 2, 0 );   /* Move to correct position */
	 addstr( outputString );      /* Output the string        */
	 
	 refresh();		      /* Draw the screen */
      }

	 exit(0);
   }

return 0;
}

int createFileLock( int fileDescriptor ) {
   
   /* Locks a file descriptor that's already been opened */

   struct flock f_lock;
	 
   f_lock.l_type = F_RDLCK; 
   f_lock.l_whence = SEEK_SET;
   f_lock.l_start = 0;
   f_lock.l_len = 0;
   
   return( ( fcntl( fileDescriptor, F_SETLK, &f_lock ) ) == -1 ); 
}

void cleanup( int mySignal ) {
      
      /* Cleans up after user breaks out of program with CTRL+C or a signal */
      
      kill( pidOfFork , SIGTERM );
      
      free( outputString );
      
      clear();
      endwin();
      curs_set( 1 );  /* Turn cursor back on */

      close( infileFD );

      exit( 0 );
   
}

void myWindowResize( int mySignal ) {
  
   /* Function resizes window and reallocates memory for line buffer */
   
   char *tempString;
 
   getScreenSize();  /* Get new screen size                              */
   clear(); 	     /* Clear the screen                                 */
   endwin();         /* End the current curses window, in case it's open */ 

   tempString = ( char * )realloc( outputString,  
				    sizeof( char ) * ( SCREENCOLS + 1 ) );

   if( tempString ) {
      outputString = tempString;
      bzero( outputString, SCREENCOLS + 1 );  /* change string to null */
      memset( outputString, ' ', SCREENCOLS );/* terminated string of  */
						 /* spaces                */

      lseek( infileFD, 0, SEEK_SET );	 /* Seek to beginning of */
					 /* file, to start over. */
      initscr();
      clear();
   }
   else {
      fprintf( stderr, "Could not realloc memory.\n" );
      close( infileFD );
      exit( 1 );
   }  
}

void getScreenSize( void ) {
   /* Fetches number of rows and columns of current screen, placing them */
   /* into global variables SCREENROWS and SCREENCOLS.  The technique is */
   /* from more04.c, written by Bruce Molay.                             */

   struct winsize windowAttr;	/* storage struct for window params */
   int fd_tty = -1;		/* file descriptor for tty ioctl    */

   fd_tty = open( "/dev/tty", O_RDONLY );
   if( fd_tty == -1 )
      exit( 1 );

   if( ioctl( fd_tty, TIOCGWINSZ, &windowAttr ) != -1 ) {
      SCREENROWS = windowAttr.ws_row - 1;
      SCREENCOLS = windowAttr.ws_col;
   }
   else {
      close( fd_tty );
      perror( "getScreenSize" );
      exit( 1 );
   }
   
   close( fd_tty );

}
