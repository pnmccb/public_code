//Implementation file for CS220 project #6
//Written by Paul McCubbins
#include <iostream.h>
#include <string.h>
#include "project6.h"

btree_array::btree_array( int init_size=1) {
   // Straightforward function, simply copies the size
   // from the function's constructor and declares an
   // array of suitable size.

   this->size = init_size;

   this->array = new btree_data_type[ init_size ];

}

void btree_array::fill( btree_data_type *contents ) {
   //Here's my quick-and-dirty fill routine.  Since array-based
   //binary search trees work best as filled and balanced trees,
   //I'll just fill the array wholesale with a predetermined string.
   //There's no need to bzero since we create an array and immediately
   //fill it entirely.  This is not a **sorted** binary tree, mind you.
   
   memcpy( this->array, contents, size );

}

void btree_array::see() {
   //Function is used mostly for debugging.  Simply steps though
   //each cell in the array and displays it, along with the index number.
   
   for( int j = 0; j < this->size; ++j )
      cout << j << "\t" << this->array[j] << endl;

}

int btree_array::left( int parent_index ) {
   // The project specs said "return the left child."  Is that the data
   // or the address?
   
   return( 2 * parent_index + 1 );

}

btree_array btree_array::parent( int parent_index ) {
   // The project specs said "return the parent."  Is that the data
   // or the address?
  
   return( (btree_array) this->array[ parent_index ] );

}

int btree_array::right( int parent_index ) {
   // The project specs said "return the right child."  Is that the data
   // or the address?
  
   return( 2 * parent_index + 2 );

}

void btree_array::inorder( int cursor_index ) {
   
   // Are we at the bottom-most left node?
   if ( cursor_index >= this->size )
      return;

   // Recurse for the left child:
   this->inorder( 2 * cursor_index + 1 );
   
   cout << this->array[ cursor_index ] << endl;
   
   // Recurse for the right child:
   this->inorder( 2 * cursor_index + 2 );

}

void btree_array::preorder( int cursor_index ) {
  
   // Are we at the bottom-most left node?
   if ( cursor_index >= this->size )
      return;

   cout << this->array[ cursor_index ] << endl;
   
   // Recurse for the left child:
   this->preorder( 2 * cursor_index + 1 );
   
   // Recurse for the right child:
   this->preorder( 2 * cursor_index + 2 );

}

