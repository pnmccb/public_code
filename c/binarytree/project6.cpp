// Main program file for CS220 project #6
// Written by Paul McCubbins
#include <iostream.h>
#include "project6.h"

btree_data_type *TREE_FILLER = "abcdefghijklmn";

int main()
{

    btree_array *project6 = new btree_array(strlen(TREE_FILLER));

    project6->fill(TREE_FILLER);

    cout << "Tree contains:" << endl << TREE_FILLER << endl << endl;

    cout << "Example of in-order traversal:" << endl;
    project6->inorder( 0 );

    cout << endl << "Example of pre-order traversal:" << endl;
    project6->preorder( 0 );

    delete project6;

    return 0;
}
