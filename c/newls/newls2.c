/* ls2.c
 *	purpose  list contents of directory or directories
 *	action   if no args, use .  else list files in args
 *	note     uses stat and pwd.h and grp.h 
 *	BUG: try ls2 /tmp 
 *newls2.c ( Paul McCubbins ), CS 306 homeworks 9 and 10, due 3/28/03
 *	- listing subdirs is fixed ( homework #9 );
 *	- handles suid, guid, and sticky bits correctly	( homework #10 );
 */
#include	<stdio.h>
#include	<stdlib.h>
#include	<sys/types.h>
#include	<dirent.h>
#include	<sys/stat.h>
#include	<string.h>

void do_ls(char[]);
void dostat(char *);
void show_file_info( char *, struct stat *);
void mode_to_letters( int , char [] );
char *uid_to_name( uid_t );
char *gid_to_name( gid_t );
char *myBaseName( char *pathName ); 
void sortArray( char **, int *elements );

int main(int ac, char *av[])
{
   if ( ac == 1 )
	    do_ls( "." );
   else
      while ( --ac ){
	 printf("%s:\n", *++av );
	 do_ls( *av );
      }
   
   exit( SUCCESS );
}

void do_ls( char dirname[] )
/*
 *	list files in directory called dirname
 */
{
   DIR                *dir_ptr;		/* the directory */
   struct dirent      *direntp;		/* each entry	 */
   
   char **dirList     = NULL;		/* directory list */
   
   char *dirEntry     = NULL;		/* String for each
					   directory entry */
   
   char *tempDirEntry = NULL;		/* ptrs for */ 
   char **tempDirList = NULL;		/* test storage */
   
   
   unsigned int dirSize = 0;
   int j;
   
   if ( ( dir_ptr = opendir( dirname ) ) == NULL )
      fprintf(stderr,"ls1: cannot open %s\n", dirname);
   else {
      while ( ( direntp = readdir( dir_ptr ) ) != NULL ) {

	 /* I know, I know.  This should all be in a function.  Please see */
	 /* my README in the upload directory                              */
	
	 /* Test-malloc some memory for the new directory string, plus one */
	 /* for '/' and another for '\0'				   */
	 tempDirEntry = realloc( dirEntry, 
	                     ( strlen( direntp->d_name ) + 
			       strlen( dirname) + 2 ) );
	 
	 if( tempDirEntry )
	    dirEntry = tempDirEntry;	/* It's OK */
	 
	 /* In the next section, just cram the pathname, a slash, and the  */
	 /* directory name into the newly allocated memory: 		   */
	 sprintf( dirEntry, "%s/%s", dirname, direntp->d_name );

	 // Test-allocate a new row:
	 tempDirList = realloc( dirList, sizeof( char ** ) * ( dirSize + 1 ) );

	 if( tempDirList )
	    dirList = tempDirList;	/* It's OK */
	 
	 // Allocate space for the string ( column ), plus one for the \0:
	 dirList[ dirSize ] = malloc( ( 
		  			strlen( dirEntry ) + 1 ) * 
	       				sizeof( char * ) );

	 // Put the directory entry into the list:
	 strcpy( dirList[ dirSize ], dirEntry );

	 // Increment the size of the directory list:
	 ++dirSize;

      }

      sortArray( dirList, &dirSize );
      
      closedir(dir_ptr);
      
      /* Display the data: */
      for( j = 0; j < dirSize; j++ ) {
	 dostat( dirList[ j ] );
      }
     
      /* Cleanup */
      for( j = 0; j < dirSize; j++ ) { 
	 free( dirList[ j ] );
      }
      
      free( dirList );
      free( dirEntry );
   }
}

void sortArray( char **array, int *numElements ) {

   /* This is a bubble sort.  Yes, I'm terribly embarassed. */
   /* Please see the README in the upload dir               */

   char *tempBuffer;
   int sortFlag = -1;
   int j;

   do {

      sortFlag = -1;
     
      for( j = 0; j < (*numElements) - 1;  ++j ) {

	 if( strcmp( array[ j ], array[ j + 1 ] ) > 0 ) {

	    sortFlag = 1;
	    
	    tempBuffer = malloc( sizeof( char ) * strlen( array[ j ] ) + 1 );
	    strcpy( tempBuffer, array[ j ] );
	    
	    array[ j ] = realloc( array[ j ], ( strlen( array[ j + 1] ) + 1 ) * sizeof( char ) );
	    strcpy( array[ j ], array[ j + 1 ] );
	    
	    array[ j + 1 ] = realloc( array[ j + 1 ], ( strlen( tempBuffer ) + 1 ) * sizeof( char ) );
	    strcpy( array[ j + 1 ], tempBuffer );

	    free( tempBuffer );
	 }
      }

   } while( sortFlag == 1 ); 
   
}

void dostat( char *filename )
{
   struct stat info;

   if ( stat(filename, &info) == -1 )		/* cannot stat	  */
      perror( filename );			/* say why	  */
   else						/* else show info */
      show_file_info( filename, &info );
}

char *myBaseName( char *pathName ) {

   char *tempPathName = strdup( pathName );

   if( tempPathName == NULL ) {
      fprintf( stderr,"ls1: cannot allocate memory for basename %s\n", pathName );
      exit(3);
   }
  
   /* Keep inserting nulls at the end of the string until we get to the	 */
   /* first one - in case something stupid happens, like multiply '/'s   */
   /* on the end of the line:    	 				 */
   while( tempPathName[ strlen( tempPathName ) - 1 ] == '/' )
      tempPathName[ strlen( tempPathName ) - 1 ] = '\0';

   tempPathName = strrchr( pathName, '/' );
   
   return ( tempPathName );
   
}

void show_file_info( char *filename, struct stat *info_p )
/*
 * display the info about 'filename'.  The info is stored in struct at *info_p
 */
{
   char	*uid_to_name(), *ctime(), *gid_to_name(), *filemode();
   void	mode_to_letters();
   char modestr[11];
   char *baseDirName = myBaseName( filename );

   mode_to_letters( info_p->st_mode, modestr );

   printf( "%s"    , modestr );
   printf( "%4d "  , (int) info_p->st_nlink);	
   printf( "%-8s " , uid_to_name(info_p->st_uid) );
   printf( "%-8s " , gid_to_name(info_p->st_gid) );
   printf( "%8ld " , (long)info_p->st_size);
   printf( "%.12s ", 4+ctime(&info_p->st_mtime));
   printf( "%s\n"  , 1+myBaseName( filename ) );

}

/*
 * utility functions
 */

/*
 * This function takes a mode value and a char array
 * and puts into the char array the file type and the 
 * nine letters that correspond to the bits in mode.
 * NOTE: It does not code setuid, setgid, and sticky
 * codes
 */
void mode_to_letters( int mode, char str[] )
{
   strcpy( str, "----------" );           /* default=no perms */

   if ( S_ISDIR(mode) )  str[0] = 'd';    /* directory?       */
   if ( S_ISCHR(mode) )  str[0] = 'c';    /* char devices     */
   if ( S_ISBLK(mode) )  str[0] = 'b';    /* block device     */

   if ( mode & S_IRUSR ) str[1] = 'r';    /* 3 bits for user  */
   if ( mode & S_IWUSR ) str[2] = 'w';
   if ( mode & S_IXUSR ) str[3] = 'x';

   if ( mode & S_ISUID && str[3] == 'x' ) 
      str[3] = 's';			  /* If executable & suid */
   else if( mode & S_ISUID )
      str[3] = 'S';                       /* If not executable & suid */

   if ( mode & S_IRGRP ) str[4] = 'r';    /* 3 bits for group */
   if ( mode & S_IWGRP ) str[5] = 'w';
   if ( mode & S_IXGRP ) str[6] = 'x';
   
   if ( mode & S_ISGID && str[6] == 'x' ) 
      str[6] = 's';
   else if( mode & S_ISGID )
      str[6] = 'S';

   if ( mode & S_IROTH ) str[7] = 'r';    /* 3 bits for other */
   if ( mode & S_IWOTH ) str[8] = 'w';
   if ( mode & S_IXOTH ) str[9] = 'x';
   
   if ( mode & S_ISVTX ) str[9] = 't';   /* Sticky bit */
}

#include	<pwd.h>

char *uid_to_name( uid_t uid )
/* 
 *	returns pointer to username associated with uid, uses getpw()
 */	
{
   struct	passwd *getpwuid(), *pw_ptr;
   static  char numstr[10];

   if ( ( pw_ptr = getpwuid( uid ) ) == NULL ){
      sprintf(numstr,"%d", uid);
      return numstr;
   }
   else
      return pw_ptr->pw_name ;
}

#include	<grp.h>

char *gid_to_name( gid_t gid )
/*
 *	returns pointer to group number gid. used getgrgid(3)
 */
{
   struct group *getgrgid(), *grp_ptr;
   static  char numstr[10];

   if ( ( grp_ptr = getgrgid(gid) ) == NULL ){
      sprintf(numstr,"%d", gid);
      return numstr;
   }
   else
      return grp_ptr->gr_name;
}
