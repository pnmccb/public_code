#!/bin/bash
. /etc/make.conf

cd /home/ftp
rm -rf /home/ftp/* 2>/dev/null >/dev/null

FILE=`/usr/local/bin/getsnapshot.php $GENTOO_MIRRORS /gentoo/snapshots/*.bz2`
TARFILE=`basename $FILE`

rm -rf /usr/portage
cd /usr
tar -jxf /home/ftp/${TARFILE}
emerge --nospinner -q metadata regen
emerge -Dup world
