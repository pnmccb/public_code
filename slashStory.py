from bs4 import BeautifulSoup
from datetime import datetime, date, timedelta
import locale
import MySQLdb
import re
import string
import sys
import time
import urllib2
# Cookie: 27277%3A%3AYF9KlerPeORlJux6GrbHMs

# Does the actual HTTP GET operation
# ARGS: storyDate, datetime object in YYYYMMDD format
# RETURNS: rawHTML string containing raw HTML
def getPage( storyDate ):

	requestURL = "http://www.slashdot.org/index2.pl?issue=%s" % ( ( datetime.strftime( storyDate, "%Y%m%d" ) ) )

	print( "RequestURL is %s" % requestURL )

	req = urllib2.Request( requestURL )
	#req.add_header( "Cookie", "user=27277%3A%3AnZtehL8U5rcZ4MeC13QNrX" )
	req.add_header( "Cookie", "user=27277%3A%3Ase6ljs3sWn6ozB2dazpn3X" )
        req.add_header( "User-Agent", "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/0.9.8q" )

	rawHTML = urllib2.urlopen( req, "", 60 ).read()

	return( rawHTML )

#------------------------------------------------------------------------------------------------------------

# Parses the HTML file into a dictionary containing various elements.
# ARGS: page is a string containing the raw HTML page
# RETURNS: dictionary containing story elements ( title, submittedby, date, department, link )
#          r[ 'title ' ]
#          r[ 'link' ]
#          r[ 'author' ]
#          r[ 'dateandtime' ]
#          r[ 'department' ]
def parseStories( page ):

    storyList = {};

    parse = BeautifulSoup( page )
    # big "O" notation is about to have a heart attack:

    title_pattern = re.compile('^title-(\d+)');
    for span in parse.find_all(id=title_pattern):
        story_id = title_pattern.search(span.get('id')).group(1);
        href = span.find_all('a')[0];
        story_title = string.strip(href.text);
        story_link  = 'http:' + href['href'];

        storyList[story_id] = { 'id': story_id, 'title': story_title, 'link': story_link }

    details_pattern  = re.compile('^details-(\d+)');
    author_pattern   = re.compile('(Submitted|Posted)\s+by\s+(.*)');
    for div in parse.find_all(id=details_pattern):
        story_id = details_pattern.search(div.get('id')).group(1);
        storyList[story_id]['author']      = author_pattern.search(div.text).group(2);
        storyList[story_id]['dateandtime'] = div.find_all('time')[0].text[3:];
        storyList[story_id]['department']  = string.strip(div.find_all('span', 'dept-text')[0].text);

    text_pattern  = re.compile('^text-(\d+)');
    for div in parse.find_all(id=text_pattern):
        story_id = text_pattern.search(div.get('id')).group(1);
        storyList[story_id]['body']        = string.strip(div.text);

    print( "%i stories parsed." % ( len( storyList ) ) )

    return( storyList )

#------------------------------------------------------------------------------------------------------------

# Test output generator;
# ARGS: dictionary returned by getStories
# RETURNS: nothing; outputs to STDOUT
def outputStories( stories ):
    for story in stories:

        # Parse out the date:
        # Postedby Soulskill on 2012-12-08  9:29 from the my-enemy-my-ally dept.

        print( "----story----" )
        print( "%s ----> %i" % ( story[ 'title' ], len( story[ 'title' ] ) ) )
        print( "%s" % story[ 'link' ] )
        print( "details:" )
        print( "\t%s\n\t%s\n\t%s" % ( story[ 'author' ], story[ 'dateandtime' ], story[ 'department' ] ) )
        print( "details:" )
        print( story[ 'body' ] )
        print( "----story----\n" )

#------------------------------------------------------------------------------------------------------------
# Takes stories put into dict by parseStories, and puts them into the MySQL db.
# ARGS: stories dict
#       stories[ 'title' ]
#       stories[ 'link' ]
#       stories[ 'dateandtime' ]
#       stories[ 'author' ]
#       stories[ 'department' ]
#       stories[ 'body' ]
# RETURNS nothing.
def doSQL( stories ):

    insertQuery = "INSERT INTO slashdot.stories (id, date,title,link,author,dept,intro) VALUES (%s, %s, %s, %s, %s, %s, %s)"

    db = MySQLdb.connect( host="172.16.3.1", user="slashdot", passwd="sN0t",db="slashdot" )
    cur = db.cursor()

    for story in stories.values():

        storyTitle = story[ 'title' ].encode("utf-8", 'ignore')

        cur.execute( "SELECT title from slashdot.stories where title=%s and id=%s", [ storyTitle, story['id'] ] )
        test = cur.fetchone()


        try:
            sys.stdout.write( test[ 0 ] + " " + story[ 'link' ] )


        except TypeError:

            # Story does not exist
            # db is date,title,link,author,department,intro

            cur.execute( insertQuery, ( story[ 'id' ],
                                        story[ 'dateandtime' ],
                                        storyTitle,
                                        story[ 'link' ],
                                        story[ 'author' ],
                                        story[ 'department' ],
                                        story[ 'body' ] ) )

            try:
               sys.stdout.write( story['title'] + " " + story[ 'link' ] + " O\n\n" )
            except:
               sys.stdout.write( story[ 'link' ] + "O\n\n" )

        except:
            sys.stdout.write( story[ 'link' ] )

        else:
            sys.stdout.write( " X\n\n" )

    db.commit()

#------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------------------
# end of functions, start of main codeblock
#------------------------------------------------------------------------------------------------------------

try:
	locale.setlocale( locale.LC_ALL, 'US' )
except locale.Error:
	locale.setlocale( locale.LC_ALL, 'en_US' )

if( len( sys.argv ) == 1 ):
    # left off beginning and ending date, so put in yesterday's date

    beginningDate = datetime.today() - timedelta( days=1 )
    endingDate = beginningDate

elif( len( sys.argv ) == 2 ):
    # Left off ending date (we hope), so substitute today's date
    beginningDate = datetime.strptime( sys.argv[ 1 ], '%m-%d-%Y' )
    endingDate = datetime.today()

elif( len( sys.argv ) == 3 ):
    # Beginning and ending date are there
    beginningDate = datetime.strptime( sys.argv[ 1 ], '%m-%d-%Y' )
    endingDate = datetime.strptime( sys.argv[ 2 ], '%m-%d-%Y' )

print( "Beginning date: %s\tEnding date: %s" % ( beginningDate, endingDate ) )

while( True ):

    print( "Date: %s" % datetime.strftime( beginningDate, '%m-%d-%Y' ) )

    if( beginningDate <= endingDate ):
        doSQL( parseStories( getPage( beginningDate ) ) )
        #parseStories( getPage( beginningDate ) )
        #outputStories( parseStories( getPage( beginningDate ) ) )
        #print( getPage( beginningDate ) )
        beginningDate += timedelta( days=1 )
    else:
        break


