import os
import sys
import glob
from distutils import file_util
import string

def exceptHandler( type, value, traceback ):
    if type == IndexError:
        print( "USAGE: %s filename path" % sys.argv[ 0 ] )
    elif value == 'SearchFileNotFound':
        print( "No instance found." )    
    else:
        print( "error value:%s\n%s" % ( value, traceback ) )

#===============================================================================
            
def findInSubdirectory( filename, subdirectory='' ):

    results = []

    if subdirectory:
        path = subdirectory
    else:
        path = os.getcwd()

    for root, dirs, names in os.walk( path ):

    # You'll lose the recursiveness if you just return the result of iglob!!
    
        for globItem in glob.iglob( os.path.join( root, filename ) ):
            results.append( globItem )

    if( len( results ) ):
        return results
    else:
        raise RuntimeError( "No instance found." )

#===============================================================================

sys.excepthook = exceptHandler

files = findInSubdirectory( sys.argv[ 1 ], sys.argv[ 2 ] )

for foundFile in files:
    print( foundFile )      
