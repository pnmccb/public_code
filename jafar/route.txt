Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
131.230.201.0   *               255.255.255.128 U     0      0        0 eth1
131.230.199.128 *               255.255.255.128 U     0      0        0 eth0
default         si-airport00r.i 0.0.0.0         UG    0      0        0 eth0   