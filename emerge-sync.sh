#!/bin/bash

# Find the latest Portage snapshot file, download it, and unpack
# it.  Uses getsnapshot.p(l|hp).  Bonus points for sharing the
# portage tree with rsync so that other internal Gentoo machines
# can update off of this one and don't waste internet bandwidth.

. /etc/make.conf  # Get the GENTOO_MIRRORS var
TARFILE_LOC="/home/ftp/portage"; # Store the archive and MD5 temporarily.

# Testing stuff
#GENTOO_MIRRORS="ftp://satan.internalnet.zzz/portage"
#TARFILE_LOC=".";

CURL_OPTS="-s";

cd $TARFILE_LOC;
TARFILE=`curl -sl ${GENTOO_MIRRORS}/snapshots/ | /usr/local/bin/getsnapshot.pl`

if [ -f ${TARFILE_LOC}/${TARFILE} ]; then
    # We already have downloaded the proper archive:
    echo "Failed to download file; already exists in ${TARFILE_LOC}.";
    exit 1;
elif [ "${TARFILE}" == "" ]; then
    # The listing failed:
    echo "Failed to download index file.";
    exit 2;
else
    # Get rid of everything old, so we don't clutter ${TARFILE_LOC}
    rm -f ${TARFILE_LOC}/*
fi

# Next, use curl to get the archive and corresponding md5 sum. While we're
# at it, make a log of our activities. (Gentoo mirrors have snapshot files
# that follow this algorithm):
# portage-[year][month][day].tar.bz2
# portage-[year][month][day].tar.bz2.md5sum

for i in ${TARFILE}.md5sum ${TARFILE}; do
    echo ${i} >> ${TARFILE_LOC}/log.txt;
    curl -w '----\nDownload size:\t%{size_download}\nDownload time:\t%{time_total}\nDownload speed:\t%{speed_download}\n\n' ${CURL_OPTS} ${GENTOO_MIRRORS}/snapshots/${i} -o ${i} >> ${TARFILE_LOC}/log.txt
done;

# Check the md5sum of the package, just in case the download terminated
# unexpectedly, or didn't happen at all, or something like that.  If the
# md5sum is correct, blow away the existing portage tree and unpack the
# archive in its place.  After that, do a regen and metadata update, 
# then display the contents of the glsa archive, and the outdated packages:
if md5sum -c --status ${TARFILE}.md5sum; then
   rm -rf /usr/portage
   cd /usr
   tar -jxf ${TARFILE_LOC}/${TARFILE};
   emerge --nospinner -q metadata regen;
   glsa-check -nl | grep "\[N\]";
   emerge -Dup world;
else
   echo "Failed.";
   exit 1;
fi

